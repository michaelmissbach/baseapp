<?php

namespace BaseApp\BaseappBundle\Gui\Elements;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class Alerts
 * @package BaseApp\BaseappBundle\Gui\Elements
 */
class Alerts extends GuiView
{
    protected $template = '@Baseapp/alerts/header.html.twig';
}