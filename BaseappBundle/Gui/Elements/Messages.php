<?php

namespace BaseApp\BaseappBundle\Gui\Elements;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class Messages
 * @package BaseApp\BaseappBundle\Gui\Elements
 */
class Messages extends GuiView
{
    protected $template = '@Baseapp/message/partials/header/messages.html.twig';
}