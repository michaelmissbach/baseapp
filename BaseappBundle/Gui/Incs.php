<?php

namespace BaseApp\BaseappBundle\Gui;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Blank;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\ExternStyleSheet;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternStyleSheet;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGui;

/**
 * Incs
 */
class Incs implements IGui
{
    /**
     * @param GuiBuilder $guiBuilder
     */
    public function create(GuiBuilder $guiBuilder): void
    {
        $headMetas = $guiBuilder->getHeadMetas();
        $headMetas->add(Blank::create()->setTitle('<meta charset="utf-8">'));
        $headMetas->add(Blank::create()->setTitle('<meta http-equiv="X-UA-Compatible" content="IE=edge">'));
        $headMetas->add(Blank::create()->setTitle('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">'));
        $headMetas->add(Blank::create()->setTitle('<meta name="description" content="">'));
        $headMetas->add(Blank::create()->setTitle('<meta name="author" content="">'));

        $headStyle = $guiBuilder->getHeadStyleSheets();
        $headStyle->add(InternStyleSheet::create()->setTitle('/bundles/baseapp/vendor/fontawesome-free/css/all.min.css'));
        $headStyle->add(ExternStyleSheet::create()->setTitle('https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'));
        $headStyle->add(InternStyleSheet::create()->setTitle('/bundles/baseapp/css/sb-admin-2.min.css'));
        $headStyle->add(InternStyleSheet::create()->setTitle('/bundles/baseapp/css/custom.css'));
        $headStyle->add(InternStyleSheet::create()->setTitle('/bundles/baseapp/css/jquery-ui.css'));
        $headStyle->add(InternStyleSheet::create()->setTitle('/bundles/baseapp/vendor/datatables/dataTables.bootstrap4.min.css'));

        $headJs = $guiBuilder->getHeadJavascripts();
        $headJs->add(InternJavascript::create()->setTitle('/bundles/baseapp/vendor/jquery/jquery.min.js'));
        $headJs->add(InternJavascript::create()->setTitle('/bundles/baseapp/js/jquery-ui.js'));
        $headJs->add(InternJavascript::create()->setTitle('/bundles/baseapp/vendor/bootstrap/js/bootstrap.bundle.min.js'));
        $headJs->add(InternJavascript::create()->setTitle('/bundles/baseapp/vendor/jquery-easing/jquery.easing.min.js'));
        $headJs->add(InternJavascript::create()->setTitle('/bundles/baseapp/vendor/datatables/jquery.dataTables.min.js'));
        $headJs->add(InternJavascript::create()->setTitle('/bundles/baseapp/vendor/datatables/dataTables.bootstrap4.min.js'));
        $headJs->add(InternJavascript::create()->setTitle('/bundles/baseapp/js/sb-admin-2.js'));
        $headJs->add(InternJavascript::create()->setTitle('/bundles/baseapp/js/baseapp.js'));
        $headJs->add(InternJavascript::create()->setTitle('/bundles/baseapp/js/logout.js'));
    }

    /**
     * @param GuiBuilder $guiBuilder
     */
    public function modify(GuiBuilder $guiBuilder): void
    {
    }
}
