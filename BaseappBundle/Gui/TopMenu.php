<?php

namespace BaseApp\BaseappBundle\Gui;

use BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu\Level1Divider;
use BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu\Level1MenuEntryWithChilds;
use BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu\Level2Divider;
use BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu\Level2MenuEntry;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGui;
use BaseApp\BaseappBundle\Gui\Elements\Alerts;
use BaseApp\BaseappBundle\Gui\Elements\Messages;
use BaseApp\BaseappBundle\Service\UserService;

/**
 * TopMenu
 */
class TopMenu implements IGui
{
    /**
     * @param GuiBuilder $guiBuilder
     */
    public function create(GuiBuilder $guiBuilder): void
    {
        if (!UserService::$instance->isLoggedin()) {
            return;
        }

        $topMenu = $guiBuilder->getTopMenu();

        $topMenu->add(Alerts::create()->setName('baseapp_alerts'),50);
        $topMenu->add(Messages::create()->setName('baseapp_messages'),75);

        $topMenu->add(Level1Divider::create()->setName('baseapp_top_divider'),100);

        $container = $guiBuilder->createGuiContainer();
        $topMenu->add(
            Level1MenuEntryWithChilds::create()
            ->setChildContainer($container)
            ->setTitle(UserService::$instance->user()->getUsername())
            ->setName('baseapp_user_menu')
            ->setIcon('fas fa-user fa-sm fa-fw mr-2 text-gray-400'),
            125
        );  
        
        $container->add(
            Level2MenuEntry::create()
            ->setTitle('Profile')
            ->setName('profile')
            ->setRouteName('baseapp_profile')  
            ->setIcon('fas fa-user fa-sm fa-fw mr-2 text-gray-400')    ,
            10
        );

        $container->add(
            Level2MenuEntry::create()
                ->setTitle('Change password')
                ->setName('change_password')
                ->setRouteName('baseapp_password')
                ->setIcon('fas fa-user fa-sm fa-fw mr-2 text-gray-400'),
            20
        );

        $container->add(
            Level2MenuEntry::create()
                ->setTitle('Settings')
                ->setName('settings')
                ->setRouteName('baseapp_settings_edit')
                ->setIcon('fas fa-cogs fa-sm fa-fw mr-2 text-gray-400'),
            30
        );
        $container->add(
            Level2MenuEntry::create()
                ->setTitle('Groups')
                ->setName('groups')
                ->setRouteName('baseapp_group_index')
                ->setIcon('fas fa-users fa-sm fa-fw mr-2 text-gray-400'),
            40
        );
        $container->add(
            Level2MenuEntry::create()
                ->setTitle('Users')
                ->setName('users')
                ->setRouteName('baseapp_user_index')
                ->setIcon('fas fa-list fa-sm fa-fw mr-2 text-gray-400'),
            50
        );
        $container->add(
            Level2Divider::create()
            ->setName('divider'),
            60
        );
        $container->add(
            Level2MenuEntry::create()
                ->setTitle('Websocket')
                ->setName('websocket')
                ->setRouteName('baseapp_websocket')
                ->setIcon('fas fa-list fa-sm fa-fw mr-2 text-gray-400'),
            70
        );
        $container->add(
            Level2Divider::create()
                ->setName('divider2'),
            80
        );
        $container->add(
            Level2MenuEntry::create()
                ->setTitle('Logout')
                ->setName('logout')
                ->setRouteName('')
                ->setIcon('fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400')
                ->setLinkClass('baseapp-modal-dialog')
                ->addLinkDataTag('href',UserService::$instance->getRoute('baseapp_logout'))
                ->addLinkDataTag('title','Exit?')
                ->addLinkDataTag('body','Do you want to logout?')
                ->addLinkDataTag('button','Ok'),
            255
        );
    }

    /**
     * @param GuiBuilder $guiBuilder
     */
    public function modify(GuiBuilder $guiBuilder): void
    {
    }
}
