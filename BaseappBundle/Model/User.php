<?php

namespace BaseApp\BaseappBundle\Model;

/**
 * Class User
 * @package BaseApp\BaseappBundle\Model
 */
class User
{
    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $username = '';

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var array
     */
    private $groups = [];

    /**
     * @var bool
     */
    private $isAnonymous = false;

    /**
     * @var string
     */
    private $token = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }



    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAnonymous(): bool
    {
        return $this->isAnonymous;
    }

    /**
     * @param bool $isAnonymous
     * @return User
     */
    public function setIsAnonymous(bool $isAnonymous): User
    {
        $this->isAnonymous = $isAnonymous;
        return $this;
    }

    /**
     * @return array
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @return ?string
     */
    public function getGroupsAsList(): ?string
    {
        return implode(',',$this->groups);
    }

    /**
     * @param array $groups
     * @return User
     */
    public function setGroups(array $groups): User
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @param $group
     * @return User
     */
    public function addGroup($group): User
    {
        $this->groups[] = $group;
        return $this;
    }

    /**
     * @param $group
     * @return bool
     */
    public function inGroup($group)
    {
        return in_array($group,$this->getGroups());
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return User
     */
    public function setToken(string $token): User
    {
        $this->token = $token;
        return $this;
    }
}
