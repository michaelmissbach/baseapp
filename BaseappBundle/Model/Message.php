<?php

namespace BaseApp\BaseappBundle\Model;

/**
 * Class Message
 * @package BaseApp\BaseappBundle\Entity
 */
class Message
{
    /**
     * @var
     */
    private $user;

    /**
     * @var bool
     */
    private $unread = true;

    /**
     * @var bool
     */
    private $highPrio = false;

    /**
     * @var string
     */
    private $subject = '';

    /**
     * @var string
     */
    private $content = '';

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Message
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnread()
    {
        return $this->unread;
    }

    /**
     * @param mixed $unread
     * @return Message
     */
    public function setUnread($unread)
    {
        $this->unread = $unread;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHighPrio()
    {
        return $this->highPrio;
    }

    /**
     * @param mixed $highPrio
     * @return Message
     */
    public function setHighPrio($highPrio)
    {
        $this->highPrio = $highPrio;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return Message
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
}
