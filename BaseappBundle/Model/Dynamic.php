<?php

namespace BaseApp\BaseappBundle\Model;

/**
 * Class Dynamic
 * @package BaseApp\BaseappBundle\Model
 */
class Dynamic
{
    /**
     * @var array
     */
    protected $properties = [];

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return array_key_exists($name,$this->properties) ? $this->properties[$name] : null;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name,$value)
    {
        $this->properties[$name] = $value;
    }

    /**
     * @return \Generator
     */
    public function getAll(): \Generator
    {
        foreach($this->properties as $key=>$value) {
            yield $key=>$value;
        }
    }
}
