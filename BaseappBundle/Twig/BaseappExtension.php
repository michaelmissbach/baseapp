<?php

namespace BaseApp\BaseappBundle\Twig;

use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Service\AppService;
use BaseApp\BaseappBundle\Service\SettingsService;
use BaseApp\BaseappBundle\Service\StringHelper;
use BaseApp\BaseappBundle\Service\UserService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class BaseappExtension
 * @package BaseApp\BaseappBundle\Twig
 */
class BaseappExtension extends AbstractExtension
{
    /**
     * @var AppService
     */
    protected $appState;
    
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var GuiBuilder
     */
    protected $guiBuilder;

    /**
     * BaseappExtension constructor.
     * @param AppService $appState
     * @param RequestStack $requestStack
     * @param GuiBuilder $guiBuilder
     */
    public function __construct(AppService $appState, RequestStack $requestStack, GuiBuilder $guiBuilder)
    {
        $this->appState = $appState;
        $this->requestStack = $requestStack;
        $this->guiBuilder = $guiBuilder;
    }
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('baseapp', [$this, 'baseapp']),
            new TwigFunction('baseuri', [$this, 'baseuri']),
            new TwigFunction('baseapp_gui', [$this, 'getGui'], ['is_safe' => ['html']]),
            new TwigFunction('getFormRowByName', [$this, 'getFormRowByName'])
        ];
    }

    /**
     * @param string $function
     * @param array $params
     * @return array|GuiBuilder|\BaseApp\BaseappBundle\Model\User|UserService|bool|mixed
     */
    public function baseapp($function = '',$params = [])
    {
        switch($function) {
            case 'user':
                return UserService::$instance->user();

            case 'allowed_route':
                return UserService::$instance->checkAllowRoute($params['route']);

            case 'is_allowed':
                return UserService::$instance->checkAllowRoute($params['groups']);

            case 'appstate':
                return $this->appState->get()->all();

            default:
                return UserService::$instance;
        }
    }

    /**
     * @return string
     */
    public function baseuri(): string
    {
        $request = $this->requestStack->getMasterRequest();
        
        $host = $request->getSchemeAndHttpHost();
        
        $path = $request->getBasePath();
        
        $base = sprintf('%s%s',$host,$path);
        if (StringHelper::endsWith($base, '/')) {
            $base = substr($base,0,strlen($base)-1);
        }
        return $base;
    }

    /**
     * @param $form
     * @param $group
     * @param $key
     * @return mixed|null
     */
    public function getFormRowByName($form,$group,$key)
    {
        $key = SettingsService::createSemanticKey($group,$key);
        return $form[$key] ?: null;
    }

    /**
     * @return GuiBuilder
     */
    public function getGui(): GuiBuilder
    {
        $this->guiBuilder->build();

        return $this->guiBuilder;
    }
}
