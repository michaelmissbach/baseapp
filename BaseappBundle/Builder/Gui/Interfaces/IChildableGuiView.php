<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Interfaces;

use BaseApp\BaseappBundle\Builder\Gui\ChildableViewContainer;

/**
 * Interface IChildableGuiView
 * @package BaseApp\BaseappBundle\Builder\Gui
 */
interface IChildableGuiView
{
    /**
     * @return ChildableViewContainer
     */
    public function getChildContainer(): ChildableViewContainer;
}
