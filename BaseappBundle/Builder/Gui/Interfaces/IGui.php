<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Interfaces;

use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;

/**
 * Interface IGui
 * @package BaseApp\BaseappBundle\Builder\Gui
 */
interface IGui
{
    /**
     * @param GuiBuilder $guiBuilder
     */
    public function create(GuiBuilder $guiBuilder): void;

    /**
     * @param GuiBuilder $guiBuilder
     */
    public function modify(GuiBuilder $guiBuilder): void;
}
