<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Interfaces;

use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

/**
 * Interface IGuiView
 * @package BaseApp\BaseappBundle\Builder\Gui
 */
interface IGuiView
{
    /**
     * @return bool
     */
    public function isMustBeRendered(): bool;

    /**
     * @param bool $mustBeRendered
     * @return self
     */
    public function setMustBeRendered(bool $mustBeRendered);

    /**
     * @return bool
     */
    public function isActive(): bool;

    /**
     * @param bool $active
     * @return self
     */
    public function setActive(bool $active);

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getRouteName(): string;

    /**
     * @param string $routeName
     * @return self
     */
    public function setRouteName(string $routeName);

    /**
     * Get the value of linkClass
     *
     * @return  string
     */ 
    public function getLinkClass(): ?string;

    /**
     * Set the value of linkClass
     *
     * @param  string  $linkClass
     *
     * @return  self
     */ 
    public function setLinkClass(string $linkClass);
        
    /**
     * addLinkDataTag
     *
     * @param  mixed $tag
     * @param  mixed $value
     * @return self
     */
    public function addLinkDataTag(string $tag, string $value);

    /**
     * @param string $icon
     * @return self
     */
    public function setIcon(string $icon);

    /**
     * @param string $title
     * @return self
     */
    public function setTitle(string $title);

    /**
     * @param string $template
     * @return self
     */
    public function setTemplate(string $template);

    /**
     * @param Request $request
     * @return bool
     */
    public function update(Request $request): bool;

    /**
     * @param Environment $twig
     * @param array $parameters
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render(Environment $twig, $parameters = []): string;
}
