<?php

namespace BaseApp\BaseappBundle\Builder\Gui;

use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGuiView;

/**
 * Class ChildableViewContainer
 * @package BaseApp\BaseappBundle\Builder\Gui
 */
class ChildableViewContainer
{
    /**
     * @var Environment
     */
    protected $twig;
        
    /**
     * request
     *
     * @var Request
     */
    protected $request;

    /**
     * @var array
     */
    protected $list;
        
    /**
     * __construct
     *
     * @param  mixed $twig
     * @param  mixed $request
     * @return void
     */
    protected function __construct(Environment $twig, Request $request)
    {
        $this->list = [];
        $this->twig = $twig;
        $this->request = $request;
    }
        
    /**
     * create
     *
     * @param  mixed $twig
     * @param  mixed $request
     * @return self
     */
    public static function create(Environment $twig, Request $request): self
    {
        return new static($twig,$request);
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }

    /**
     * @param IGuiView $view
     * @param int $prio
     * @return bool
     */
    public function add(IGuiView $view, $prio = 0): bool
    {
        if (!array_key_exists($prio,$this->list)) {
            $this->list[$prio] = [];
        }
        $this->list[$prio][] = $view;
        return true;
    }

    public function findElementByName()
    {

    }

    public function remove()
    {
    }

    public function sorting()
    {
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        ksort($this->list);

        $activeFound = false;
        foreach($this->list as $prio=>$list) {
            foreach($list as $key=>$entry) {
                if ($entry instanceof IGuiView) {
                    if($entry->update($this->request)) {
                        $activeFound = true;
                    }
                }
            }
        }
        return $activeFound;
    }

    /**
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render(): string
    {
        $content = '';

        foreach ($this->list as $prio => $list) {
            foreach ($list as $key => $entry) {
                if ($entry instanceof IGuiView) {
                    if ($entry->isMustBeRendered()) {
                        $content = sprintf('%s%s', $content, $entry->render($this->twig));
                    }
                }
            }
        }

        return $content;
    }
}
