<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Abstracts;

use BaseApp\BaseappBundle\Builder\Gui\ChildableViewContainer;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IChildableGuiView;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGuiView;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

/**
 * Class ChildableGuiView
 * @package BaseApp\BaseappBundle\Builder\Gui\Abstracts
 */
abstract class ChildableGuiView extends GuiView implements IChildableGuiView
{
    /**
     * @var ChildableViewContainer
     */
    protected $container;

    /**
     * @param ChildableViewContainer $container
     * @return ChildableGuiView
     */
    public function setChildContainer(ChildableViewContainer $container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * @return ChildableViewContainer
     */
    public function getChildContainer(): ChildableViewContainer
    {
        return $this->container;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function update(Request $request): bool
    {
        $this->active = $this->container->update();

        return $this->active;
    }

    /**
     * @param Environment $twig
     * @param array $parameters
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render(Environment $twig, $parameters = []): string
    {
        $counter = 0;
        foreach($this->container->getList() as $prio=>$list) {
            foreach($list as $key=>$entry) {
                if ($entry instanceof IGuiView) {
                    $entry->isMustBeRendered() && $counter++;
                }
            }
        }
        if ($counter === 0) {
            return '';
        }

        $parameters = [
            'childs' => $this->container
        ];
        return parent::render($twig,$parameters);
    }
}
