<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Abstracts;

use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGuiView;
use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

/**
 * Class GuiView
 * @package BaseApp\BaseappBundle\Builder\Gui\Abstracts
 */
abstract class GuiView implements IGuiView
{    
    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var bool
     */
    protected $mustBeRendered = true;

    /**
     * @var bool
     */
    protected $active = false;
    
    /**
     * @var string
     */
    protected $template;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $routeName;

    /**
     * @var string
     */
    protected $linkClass;
    
    /**
     * linkDataTags
     *
     * @var array
     */
    protected $linkDataTags = [];

    /**
     * GuiView constructor.
     */
    protected function __construct()
    {
    }

    /**
     * @return static
     */
    public static function create()
    {
        return new static();
    }

    /**
     * @return bool
     */
    public function isMustBeRendered(): bool
    {
        return $this->mustBeRendered;
    }

    /**
     * @param bool $mustBeRendered
     * @return GuiView
     */
    public function setMustBeRendered(bool $mustBeRendered)
    {
        $this->mustBeRendered = $mustBeRendered;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return GuiView
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return GuiView
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getRouteName(): string
    {
        return $this->routeName;
    }

    /**
     * @param string $routeName
     * @return GuiView
     */
    public function setRouteName(string $routeName)
    {
        $this->routeName = $routeName;
        return $this;
    }

    /**
     * Get the value of linkClass
     *
     * @return  string
     */ 
    public function getLinkClass(): ?string
    {
        return $this->linkClass;
    }

    /**
     * Set the value of linkClass
     *
     * @param  string  $linkClass
     *
     * @return  GuiView
     */ 
    public function setLinkClass(string $linkClass)
    {
        $this->linkClass = $linkClass;

        return $this;
    }

    /**
     * @param string $icon
     * @return GuiView
     */
    public function setIcon(string $icon)
    {
        $this->options['icon'] = $icon;

        return $this;
    }

    /**
     * @param string $title
     * @return GuiView
     */
    public function setTitle(string $title)
    {
        $this->options['title'] = $title;

        return $this;
    }
        
    /**
     * addLinkDataTag
     *
     * @param  mixed $tag
     * @param  mixed $value
     * @return GuiView
     */
    public function addLinkDataTag(string $tag, string $value)
    {
        $this->linkDataTags[$tag] = $value;

        return $this;
    }

    /**
     * @param string $template
     * @return GuiView
     */
    public function setTemplate(string $template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function update(Request $request): bool
    {
        if (strlen($this->routeName)) {
            $this->active = $request->getRequestUri() == UserService::$instance->getRoute($this->routeName);
            $this->mustBeRendered = UserService::$instance->checkAllowRoute($this->routeName);
        }

        return $this->active;
    }

    /**
     * @param Environment $twig
     * @param array $parameters
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render(Environment $twig, $parameters = []): string
    {
        $parameters['instance'] = $this;
        $parameters['name'] = strlen($this->name) ? $this->name : uniqid();
        $parameters['active'] = $this->active;
        $parameters['linkClass'] = $this->linkClass ?:'';
        $parameters['linkDataTags'] = $this->linkDataTags;
        $parameters['routeName'] = strlen($this->routeName) ? $this->routeName : false;
        $parameters['mustBeRenderer'] = $this->mustBeRendered;
        $parameters['options'] = $this->options;

        $parameters = $this->extendOptions($parameters);

        return $twig->render($this->template, $parameters);
    }

    /**
     * @param $parameters
     * @return array
     */
    protected function extendOptions($parameters): array
    {
        return $parameters;
    }
}
