<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\Incs;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class InternJavascript
 * @package BaseApp\BaseappBundle\Gui\Elements\Incs
 */
class InternJavascript extends GuiView
{
    protected $template = '@Baseapp/gui/incs/internjavascript.html.twig';
}
