<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\Incs;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class ExternStyleSheet
 * @package BaseApp\BaseappBundle\Gui\Elements\Incs
 */
class ExternStyleSheet extends GuiView
{
    protected $template = '@Baseapp/gui/incs/externstylesheet.html.twig';
}
