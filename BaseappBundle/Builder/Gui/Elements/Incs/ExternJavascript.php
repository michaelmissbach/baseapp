<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\Incs;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class ExternJavascript
 * @package BaseApp\BaseappBundle\Gui\Elements\Incs
 */
class ExternJavascript extends GuiView
{
    protected $template = '@Baseapp/gui/incs/externjavascript.html.twig';
}
