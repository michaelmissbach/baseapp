<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\Incs;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class InternStyleSheet
 * @package BaseApp\BaseappBundle\Gui\Elements\Incs
 */
class InternStyleSheet extends GuiView
{
    protected $template = '@Baseapp/gui/incs/internstylesheet.html.twig';
}
