<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class Level1MenuHeadline
 * @package BaseApp\BaseappBundle\Builder\Gui\Elements
 */
class Level1MenuHeadline extends GuiView
{
    protected $template = '@Baseapp/gui/sidebar/level1menuheadline.html.twig';
}
