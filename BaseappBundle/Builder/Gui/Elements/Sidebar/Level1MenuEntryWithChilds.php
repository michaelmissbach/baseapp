<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\ChildableGuiView;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGuiView;

/**
 * Class Level1MenuEntry
 * @package BaseApp\BaseappBundle\Builder\Gui\Elements
 */
class Level1MenuEntryWithChilds extends ChildableGuiView
{
    protected $template = '@Baseapp/gui/sidebar/level1menuentrywithchilds.html.twig';
}
