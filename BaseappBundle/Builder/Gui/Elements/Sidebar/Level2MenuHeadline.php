<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class Level2MenuHeadline
 * @package BaseApp\BaseappBundle\Builder\Gui\Elements
 */
class Level2MenuHeadline extends Level1MenuHeadline
{
    protected $template = '@Baseapp/gui/sidebar/level2menuheadline.html.twig';
}
