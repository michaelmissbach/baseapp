<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGuiView;

/**
 * Class Level1MenuEntry
 * @package BaseApp\BaseappBundle\Builder\Gui\Elements
 */
class Level1MenuEntry extends GuiView
{
    protected $template = '@Baseapp/gui/sidebar/level1menuentry.html.twig';
}
