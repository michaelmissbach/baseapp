<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class Blank
 * @package BaseApp\BaseappBundle\Gui\Elements
 */
class Blank extends GuiView
{
    protected $template = '@Baseapp/gui/blank.html.twig';
}
