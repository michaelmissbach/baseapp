<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\ChildableGuiView;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGuiView;

/**
 * Class Level1MenuEntry
 * @package BaseApp\BaseappBundle\Builder\Gui\Elements
 */
class Level1MenuEntryWithChilds extends ChildableGuiView
{
    protected $template = '@Baseapp/gui/topmenu/level1menuentrywithchilds.html.twig';
}
