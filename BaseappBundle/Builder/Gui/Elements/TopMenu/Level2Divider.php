<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class Level2Divider
 * @package BaseApp\BaseappBundle\Builder\Gui\Elements
 */
class Level2Divider extends GuiView
{
    protected $template = '@Baseapp/gui/topmenu/level2divider.html.twig';
}
