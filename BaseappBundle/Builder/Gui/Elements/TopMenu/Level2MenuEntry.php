<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGuiView;

/**
 * Class Level2MenuEntry
 * @package BaseApp\BaseappBundle\Builder\Gui\Elements
 */
class Level2MenuEntry extends GuiView
{
    protected $template = '@Baseapp/gui/topmenu/level2menuentry.html.twig';
}
