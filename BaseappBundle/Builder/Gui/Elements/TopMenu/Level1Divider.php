<?php

namespace BaseApp\BaseappBundle\Builder\Gui\Elements\TopMenu;

use BaseApp\BaseappBundle\Builder\Gui\Abstracts\GuiView;

/**
 * Class Level1Divider
 * @package BaseApp\BaseappBundle\Builder\Gui\Elements
 */
class Level1Divider extends GuiView
{
    protected $template = '@Baseapp/gui/topmenu/level1divider.html.twig';
}
