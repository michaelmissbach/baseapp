<?php

namespace BaseApp\BaseappBundle\Builder\Gui;

use BaseApp\BaseappBundle\Traits\PseudoSingleton;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGui;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class GuiBuilder
 * @package BaseApp\BaseappBundle\Builder\Gui
 */
class GuiBuilder
{
    use PseudoSingleton;

    /**
     * @var Environment
     */
    protected $twig;
        
    /**
     * request
     *
     * @var Request
     */
    protected $request;

    /**
     * @var array
     */
    protected $elements = [];

    /**
     * @var ChildableViewContainer
     */
    protected $sidebar;

    /**
     * @var ChildableViewContainer
     */
    protected $topmenu;

    /**
     * @var ChildableViewContainer
     */
    protected $headMetas;
    
    /**
     * @var ChildableViewContainer
     */
    protected $headStyleSheets;

    /**
     * @var ChildableViewContainer
     */
    protected $footerStyleSheets;

    /**
     * @var ChildableViewContainer
     */
    protected $headJavascripts;

    /**
     * @var ChildableViewContainer
     */
    protected $footerJavascripts;

    /**
     * @var bool
     */
    protected $builded = false;
        
    /**
     * __construct
     *
     * @param  Environment $twig
     * @param  RequestStack $requestStack
     * @return void
     */
    public function __construct(Environment $twig, RequestStack $requestStack)
    {
        $this->twig = $twig;
        $this->request = $requestStack->getCurrentRequest();

        $this->setInstance($this);
    }

    /**
     *
     */
    public function run(): void
    {
    }

    /**
     *
     */
    public function build(): void
    {
        if (!$this->builded) {
            foreach($this->elements as $element) {
                $element->create($this);
            }
            foreach($this->elements as $element) {
                $element->modify($this);
            }
            $this->getSidebar()->update();
            $this->getTopMenu()->update();
            $this->getHeadMetas()->update();
            $this->getHeadJavascripts()->update();
            $this->getHeadStyleSheets()->update();
            $this->getFooterJavascripts()->update();
            $this->getFooterStyleSheets()->update();
            $this->builded = true;
        }
    }

    /**
     * @return ChildableViewContainer
     */
    public function createGuiContainer(): ChildableViewContainer
    {
        return ChildableViewContainer::create($this->twig,$this->request);
    }

    /**
     * @internal
     * @param IGui $gui
     */
    public function add(IGui $gui): void
    {
        $this->elements[] = $gui;
    }

    /**
     * Get request
     *
     * @return  Request
     */ 
    public function getRequest(): Request
    {
        return $this->request;
    }

   /**
     * @return ChildableViewContainer
     */
    public function getSidebar(): ChildableViewContainer
    {
        if (!$this->sidebar) {
            $this->sidebar = $this->createGuiContainer();
        }
        return $this->sidebar;
    }

    /**
     * @return ChildableViewContainer
     */
    public function getTopMenu(): ChildableViewContainer
    {
        if (!$this->topmenu) {
            $this->topmenu = $this->createGuiContainer();
        }
        return $this->topmenu;
    }

    /**
     * @return ChildableViewContainer
     */
    public function getHeadMetas(): ChildableViewContainer
    {
        if (!$this->headMetas) {
            $this->headMetas = $this->createGuiContainer();
        }
        return $this->headMetas;
    }

    /**
     * @return ChildableViewContainer
     */
    public function getHeadStyleSheets(): ChildableViewContainer
    {
        if (!$this->headStyleSheets) {
            $this->headStyleSheets = $this->createGuiContainer();
        }
        return $this->headStyleSheets;
    }

    /**
     * @return ChildableViewContainer
     */
    public function getFooterStyleSheets(): ChildableViewContainer
    {
        if (!$this->footerStyleSheets) {
            $this->footerStyleSheets = $this->createGuiContainer();
        }
        return $this->footerStyleSheets;
    }

    /**
     * @return ChildableViewContainer
     */
    public function getHeadJavascripts(): ChildableViewContainer
    {
        if (!$this->headJavascripts) {
            $this->headJavascripts = $this->createGuiContainer();
        }
        return $this->headJavascripts;
    }

    /**
     * @return ChildableViewContainer
     */
    public function getFooterJavascripts(): ChildableViewContainer
    {
        if (!$this->footerJavascripts) {
            $this->footerJavascripts = $this->createGuiContainer();
        }
        return $this->footerJavascripts;
    }
}
