<?php

namespace BaseApp\BaseappBundle\Interfaces;

/**
 * Interface IAppSettings
 * @package BaseApp\BaseappBundle\Interfaces
 */
interface IAppSettings
{
    /**
     * @return array
     */
    public function getAppSettings(): array;
}
