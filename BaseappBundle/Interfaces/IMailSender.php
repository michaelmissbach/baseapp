<?php

namespace BaseApp\BaseappBundle\Interfaces;

/**
 * Interface IMailSender
 * @package BaseApp\BaseappBundle\Interfaces
 */
interface IMailSender
{
    /**
     * @param $to
     * @param $body
     */
    public function sendPasswordForget($to, $body);

    /**
     * @param $to
     * @param $body
     */
    public function sendRegister($to, $body);

    /**
     * @param $to
     * @param $from
     * @param $subject
     * @param $body
     */
    public function send($to, $from,  $subject, $body);
}
