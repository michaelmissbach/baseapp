<?php

namespace BaseApp\BaseappBundle\Interfaces;

/**
 * Interface IAppSettingGroups
 * @package BaseApp\BaseappBundle\Interfaces
 */
interface IAppSettingGroups
{
    /**
     * @return array
     */
    public function getAppSettingGroups(): array;
}
