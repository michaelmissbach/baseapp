<?php

namespace BaseApp\BaseappBundle\Traits;

/**
 * Class PseudoSingleton
 * @package BaseApp\BaseappBundle\Abstracts
 */
trait PseudoSingleton
{
    /**
     * @var self
     */
    static $instance;

    /**
     * @param $instance
     */
    public function setInstance($instance)
    {
        self::$instance = $instance;
    }

    /**
     *
     */
    public function run(): void
    {
    }
}
