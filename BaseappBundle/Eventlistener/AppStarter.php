<?php

namespace BaseApp\BaseappBundle\Eventlistener;

use BaseApp\BaseappBundle\Service\AppStarter as AppStarterService;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Class AppStarter
 * @package BaseApp\BaseappBundle\Eventlistener
 */
class AppStarter
{   
    /**
     * @var AppStarterService
     */
    protected $appStarterService;

    /**
     * AlertStarter constructor.
     * @param AlertService $alertService
     */
    public function __construct(AppStarterService $appStarter)
    {
        $this->appStarterService = $appStarter;
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $this->appStarterService->run();
    }
}
