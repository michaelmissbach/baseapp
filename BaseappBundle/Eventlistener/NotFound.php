<?php

namespace BaseApp\BaseappBundle\Eventlistener;

use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class NotFound
 * @package App\Eventlistener
 */
class NotFound
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * CheckLogin constructor.
     * @param RouterInterface $router
     * @param UserService $userService
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        if ($event->getThrowable() instanceof NotFoundHttpException) {
            $event->setResponse(
                new Response($this->getContent(),404)
            );
        }
    }

    /**
     * @return bool|string
     */
    protected function getContent()
    {
        $url = $this->router->generate('baseapp_notfound',[],UrlGeneratorInterface::ABSOLUTE_URL);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}
