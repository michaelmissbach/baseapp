<?php

namespace BaseApp\BaseappBundle\Eventlistener;

use BaseApp\BaseappBundle\Service\AppService;
use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

/**
 * Class CheckLogin
 * @package BaseApp\BaseappBundle\Eventlistener
 */
class CheckLogin
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * CheckLogin constructor.
     * @param AppService $appState
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param ControllerEvent $event
     */
    public function onKernelController(ControllerEvent $event)
    {
        
        $attributes = $event->getRequest()->attributes;
        
        if (!$attributes->has('_allow')) {
            return;
        }
        
        if (!UserService::$instance->isAllowed($attributes->get('_allow'))) {
            
            $event->setController(function() use ($event,$attributes) {
                AppService::$instance->setRedirectOrigin($event->getRequest()->getRequestUri());
                return new RedirectResponse($this->router->generate($attributes->get('_redirect')),302);
            });
            $event->stopPropagation();
        }
    }
}
