<?php

namespace BaseApp\BaseappBundle\Eventlistener;

use BaseApp\BaseappBundle\Service\AppService;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Class AppEnd
 * @package BaseApp\BaseappBundle\Eventlistener
 */
class AppEnd
{
    /**
     * @param ResponseEvent $event
     */
    public function onKernelResponse(ResponseEvent $event)
    {
        AppService::$instance->save();
    }
}
