<?php

namespace BaseApp\BaseappBundle\DataCollector;

use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

/**
 * Class DebugCollector
 * @package BaseApp\BaseappBundle\DataCollector
 */
class DebugCollector extends DataCollector
{

    public function collect(Request $request, Response $response, \Throwable $exception = null)
    {
        $this->data = [
            'method' => $request->getMethod(),
            'acceptable_content_types' => $request->getAcceptableContentTypes(),
            'user' => UserService::$instance->user()
        ];
    }

    public function getUser()
    {
        return $this->data['user'];
    }

    public function getMethod()
    {
        return $this->data['method'];
    }



    public function getAcceptableContentTypes()
    {
        return $this->data['acceptable_content_types'];
    }

    public function reset()
    {
        $this->data = [];
    }

    public function getName()
    {
        return 'baseapp.collector';
    }
}
