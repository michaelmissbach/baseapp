<?php

namespace BaseApp\BaseappBundle\Form;

use BaseApp\BaseappBundle\Model\Message;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class MessageFormType
 * @package App\Form
 */
class MessageFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', TextType::class, [
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('subject', TextType::class, [
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('content', CKEditorType::class, [
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('highPrio', CheckboxType::class, [
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Submit'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Message::class
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'message_form';
    }
}
