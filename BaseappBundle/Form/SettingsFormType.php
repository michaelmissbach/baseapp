<?php

namespace BaseApp\BaseappBundle\Form;

use Symfony\Component\Form\AbstractType;
use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Component\Form\FormBuilderInterface;
use BaseApp\BaseappBundle\Service\SettingsService;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Class SettingsFormType
 * @package App\Form
 */
class SettingsFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($options['fields'] as $field) {
            if (UserService::$instance->isAllowed($field[SettingsService::SETTINGS_USER_GROUP])) {                            
                $builder->add(
                    SettingsService::createSemanticKey($field[SettingsService::SETTINGS_GROUP_KEY],$field[SettingsService::SETTINGS_KEY]),
                    $field[SettingsService::SETTINGS_FORM_TYPE],
                    $field[SettingsService::SETTINGS_FORM_TYPE_CONFIG]
                );
            }            
        }

        $builder
            ->add('submit', SubmitType::class, [
                'label' => 'Submit'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'fields' => []
            ]
        );

    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'settings_form';
    }
}
