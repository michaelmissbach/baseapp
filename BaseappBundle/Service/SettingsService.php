<?php

namespace BaseApp\BaseappBundle\Service;

use BaseApp\BaseappBundle\Interfaces\IAppSettingGroups;
use BaseApp\BaseappBundle\Interfaces\IAppSettings;
use BaseApp\BaseappBundle\Traits\PseudoSingleton;
use BaseApp\BaseappBundle\Entity\Setting;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class SettingsService
 * @package BaseApp\BaseappBundle\Service
 */
class SettingsService
{
    use PseudoSingleton;

    const SETTINGS_GROUP_KEY = 'settings_group_key';
    const SETTINGS_GROUP_NAME = 'settings_group_name';
    const SETTINGS_KEY = 'settings_key';
    const SETTINGS_FORM_TYPE = 'settings_form_type';
    const SETTINGS_FORM_TYPE_CONFIG = 'settings_form_type_config';
    const SETTINGS_USER_GROUP = 'settings_user_group';

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * @var bool
     */
    protected $initialized = false;

    /**
     * @var
     */
    protected $configSettings = [];

    /**
     * @var
     */
    protected $configSettingGroups = [];

    /**
     * @var ParameterBag
     */
    protected $settings;

    /**
     * Auth constructor.
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->setInstance($this);
    }

    /**
     *
     */
    protected function load(): void
    {
        $this->settings = new ParameterBag();

        /** @var Setting $entry */
        foreach ($this->doctrine->getRepository(Setting::class)->findAll() as $entry) {
            if ($entry->getPayloadkey()) {
                $this->settings->set($entry->getPayloadkey(),$entry->getPayload());
            }
        }

        $this->initialized = true;
    }

    /**
     * @internal
     * @param IAppSettingGroups $instance
     * @return bool
     * @throws \Exception
     */
    public function registerSettingGroups(IAppSettingGroups $instance): bool
    {
        return $this->registerSettingGroupsArray($instance->getAppSettingGroups());
    }

    /**
     * @internal
     * @param array $list
     * @return bool
     * @throws \Exception
     */
    public function registerSettingGroupsArray(array $list)
    {
        foreach($list as $entry) {
            if (!is_array($entry)) {
                throw new \Exception(sprintf('App settings group entry must be type array.'));
            }
            if (!array_key_exists(self::SETTINGS_GROUP_KEY, $entry) || !array_key_exists(self::SETTINGS_GROUP_NAME,$entry)) {
                throw new \Exception(sprintf('App settings group entry must have array keys "%s" and "%s".',self::SETTINGS_GROUP_KEY,self::SETTINGS_GROUP_NAME));
            }
            $this->configSettingGroups[$entry[self::SETTINGS_GROUP_KEY]] = $entry;
        }
        return true;
    }

    /**
     * @internal
     * @param IAppSettings $instance
     * @return bool
     * @throws \Exception
     */
    public function registerSettings(IAppSettings $instance): bool
    {
        return $this->registerSettingsArray($instance->getAppSettings());
    }

    /**
     * @internal
     * @param array $list
     * @return bool
     * @throws \Exception
     */
    public function registerSettingsArray(array $list): bool
    {
        foreach($list as $entry) {
            if (!is_array($entry)) {
                throw new \Exception(sprintf('App settings entry must be type array.'));
            }
            if (
                !array_key_exists(self::SETTINGS_GROUP_KEY, $entry)
                || !array_key_exists(self::SETTINGS_KEY,$entry)
                || !array_key_exists(self::SETTINGS_FORM_TYPE,$entry)
            ) {
                throw new \Exception(
                    sprintf('App settings group entry must have array keys "%s", "%s" and "%s".',
                        self::SETTINGS_GROUP_KEY,
                        self::SETTINGS_KEY,
                        self::SETTINGS_FORM_TYPE
                    )
                );
            }
            if (array_key_exists($entry[self::SETTINGS_KEY],$this->configSettingGroups)) {
                throw new \Exception(sprintf('App settings entry "%s" already registered.'));
            }
            $this->configSettings[$entry[self::SETTINGS_KEY]] = $entry;
        }
        return true;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        if (!$this->initialized) {
            $this->load();
        }

        return $this->settings->all();
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function get($key,$default = null)
    {
        if (!$this->initialized) {
            $this->load();
        }

        return $this->settings->get($key,$default);
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key): bool
    {
        if (!$this->initialized) {
            $this->load();
        }

        return $this->settings->has($key);
    }

    /**
     * @internal
     * @return array
     */
    public function getConfigSettings(): array
    {
        return $this->configSettings;
    }

    /**
     * @internal
     * @return array
     */
    public function getConfigSettingGroups(): array
    {
        return $this->configSettingGroups;
    }

    /**
     * @param $group
     * @param $key
     * @return string
     */
    public static function createSemanticKey($group,$key): string
    {
        return sprintf('%s_%s',$group,$key);
    }
}
