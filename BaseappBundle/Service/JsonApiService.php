<?php

namespace BaseApp\BaseappBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class JsonApiService
 * @package App\Service
 */
class JsonApiService
{
    /**
     * JsonApiService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $call
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function run($call,$params = [])
    {
        $call = explode(':',$call);
        if (!count($call) === 2) {
            throw new \Exception('Illegal call parameter. Expected xxx:xxx');
        }

        if (!$this->container->has($call[0])) {
            throw new \Exception(sprintf('Service "%s" not found.',$call[0]));
        }

        $service = $this->container->get($call[0]);

        if (!is_callable([$service,$call[1]],true)) {
            throw new \Exception(sprintf('Service "%s:%s" not callable.',$call[0],$call[1]));
        }

        return $service->{$call[1]}($params);
    }
}
