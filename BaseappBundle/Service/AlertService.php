<?php

namespace BaseApp\BaseappBundle\Service;

use BaseApp\BaseappBundle\Entity\Alert;
use BaseApp\BaseappBundle\Traits\PseudoSingleton;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class AlertService
 * @package App\Service
 */
class AlertService
{
    use PseudoSingleton;

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * AlertService constructor.
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->setInstance($this);
    }

    /**
     * @param $doctrine
     * @param \Exception $e
     */
    public function externExceptionLog($doctrine,$e): void
    {
        try {
            if (!$this->doctrine) {
                $this->doctrine = $doctrine;
            }

            $this->exceptionLog($e);
        } catch(\Exception $e) {}
    }

    /**
     * @param \Exception $e
     */
    public function exceptionLog($e): void
    {
        try {
            $this->log(
                'error',
                sprintf('Error: %s; File: %s; Line: %s',$e->getMessage(),$e->getFile(),$e->getLine())
            );
        } catch(\Exception $e) {}
    }

    /**
     * @param $type
     * @param $message
     */
    public function log($type,$message): void
    {
        try {            
            if ($this->doctrine && $this->isAllowedType($type)) {                
                $alert = $this->doctrine->getRepository(Alert::class)->createNew($type,$message);
                $this->doctrine->getManager()->persist($alert);
                $this->doctrine->getManager()->flush();
            }
        } catch(\Exception $e) {
        }
    }

    /**
     * @param $type
     * @return bool
     */
    public function isAllowedType($type): bool
    {
        return defined(Alert::class.'::TYPE_'.strtoupper($type));
    }
}
