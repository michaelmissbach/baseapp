<?php

namespace BaseApp\BaseappBundle\Service;

use BaseApp\BaseappBundle\Interfaces\IAppSettings;
use BaseApp\BaseappBundle\Traits\PseudoSingleton;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Class AppService
 * @package App\Service
 */
class AppService implements IAppSettings
{
    use PseudoSingleton;

    const SESSION_KEY = 'baseapp_session_key';

    /**
     * @var ParameterBag
     */
    protected $bag;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var bool
     */
    protected $disabledSetSessionLastExecution = false;

    /**
     * @var array
     */
    protected $defaults = [
        'sidebarStateOpen' => true,
        'redirectOrigin'   => null,
        'sessionLastExecution'   => null,
    ];

    /**
     * SettingsService constructor.
     * @param SessionInterface $session3
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
        $this->bag = new ParameterBag($session->get(self::SESSION_KEY,$this->defaults));
        $this->setInstance($this);
    }

    /**
     *
     */
    public function save()
    {
        $this->session->set(self::SESSION_KEY,$this->bag->all());
    }

    /**
     * @return ParameterBag
     */
    public function get()
    {
        return $this->bag;
    }


    //******************************************************************************************************************

    /**
     * @param string|null $origin
     */
    public function setRedirectOrigin(?string $origin): void
    {
        $this->bag->set('redirectOrigin',$origin);
    }

    /**
     * @return string|null
     */
    public function getRedirectOrigin(): ?string
    {
        return $this->bag->get('redirectOrigin',null);
    }

    //******************************************************************************************************************

    public function getToggledSidebarState()
    {
        return $this->bag->get('sidebarStateOpen');
    }

    public function toggleSidebar($params)
    {
        $this->bag->set('sidebarStateOpen',!$this->bag->get('sidebarStateOpen'));
    }

    //******************************************************************************************************************

    /**
     *
     */
    public function disableSetSessionLastExecution(): void
    {
        $this->disabledSetSessionLastExecution = true;
    }

    /**
     * @param ResponseEvent $event
     * @throws \Exception
     */
    public function onKernelResponse(ResponseEvent $event)
    {
        $this->isLoggedOut();

        if (!$this->disabledSetSessionLastExecution) {
            $this->setSessionLastExecution();
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isLoggedOut(): bool
    {
        $now = new \DateTime();
        if (UserService::$instance->isLoggedin()) {
            if (SettingsService::$instance->get(SettingsService::createSemanticKey('system','system_status_session_lifetime_enable'))) {
                $seconds = (int)SettingsService::$instance->get(SettingsService::createSemanticKey('system','system_status_session_lifetime'),600);
                /** @var \DateTime $lastSession */
                $lastSession = $this->bag->get('sessionLastExecution',$now);

                if ($lastSession->add(new \DateInterval(sprintf('PT%sS',$seconds))) < $now) {
                    UserService::$instance->logout();
                    $this->session->getFlashBag()->add('info',sprintf('You have been logged out due to inactivity.'));
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     */
    public function setSessionLastExecution(): void
    {
        $this->bag->set('sessionLastExecution', new \DateTime());
    }

    //******************************************************************************************************************

    public function addFlashMessage($type,$message)
    {
        $this->session->getFlashBag()->add($type,$message);
    }

    //******************************************************************************************************************

    /**
     * @return array[]
     */
    public function getAppSettings(): array
    {
        return [
            [
                SettingsService::SETTINGS_KEY => 'system_status_session_lifetime_enable',
                SettingsService::SETTINGS_GROUP_KEY => 'system',
                SettingsService::SETTINGS_FORM_TYPE => CheckboxType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'System session lifetime enabled',
                    'required' => false
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ],
            [
                SettingsService::SETTINGS_KEY => 'system_status_session_lifetime',
                SettingsService::SETTINGS_GROUP_KEY => 'system',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'System session lifetime in seconds',
                    'empty_data' => '',
                    'required' => false
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ]
        ];
    }
}
