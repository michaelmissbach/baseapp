<?php

namespace BaseApp\BaseappBundle\Service;

use BaseApp\BaseappBundle\Interfaces\IMailSender;

/**
 * Class MailSender
 * @package BaseApp\BaseappBundle\Service
 */
class MailSender implements IMailSender
{
    /**
     * @var string
     * @todo this should come from settings
     */
    protected $from = 'baseapp@test.com';

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * MailSender constructor.
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param $to
     * @param $body
     */
    public function sendPasswordForget($to, $body)
    {
        $this->send($to,$this->from,'Password reset',$body);
    }

    /**
     * @param $to
     * @param $body
     */
    public function sendRegister($to, $body)
    {
        $this->send($to,$this->from,'Register',$body);
    }

    /**
     * @param $to
     * @param $from
     * @param $subject
     * @param $body
     */
    public function send($to, $from,  $subject, $body)
    {
        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody($body,'text/html');

        $this->mailer->send($message);
    }
}
