<?php

namespace BaseApp\BaseappBundle\Service;

use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Service\AlertService;
use BaseApp\BaseappBundle\Service\SettingsService;

class AppStarter
{
    /**
     * @var AlertService
     */
    protected $alertService;

    /**
     * @var SettingsService
     */
    protected $settingsService;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var GuiBuilder
     */
    protected $guiBuilder;

    /**
     * @var AppService
     */
    protected $appState;

    /**
     * AppStarter constructor.
     * @param SettingsService $settingsService
     * @param AlertService $alertService
     * @param UserService $userService
     */
    public function __construct(SettingsService $settingsService,AlertService $alertService,UserService $userService,GuiBuilder $guiBuilder, AppService $appState)
    {
        date_default_timezone_set('Europe/Berlin');
        $this->settingsService = $settingsService;
        $this->alertService = $alertService;
        $this->userService = $userService;
        $this->guiBuilder = $guiBuilder;
        $this->appState = $appState;
    }

    /**
     *
     */
    public function run(): void
    {
        $this->settingsService->run();
        $this->alertService->run();
        $this->userService->run();
        $this->guiBuilder->run();
        $this->appState->run();
    }        
}
