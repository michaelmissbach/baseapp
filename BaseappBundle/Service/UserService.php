<?php

namespace BaseApp\BaseappBundle\Service;

use BaseApp\BaseappBundle\Model\User;
use BaseApp\BaseappBundle\Traits\PseudoSingleton;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class UserService
 * @package App\Service
 */
class UserService
{
    use PseudoSingleton;

    const SESSION_KEY = 'user_session_key';

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
     */
    protected $session;

    /**
     * @var User
     */
    protected $anonymous;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var
     */
    protected $routeCollection;

    /**
     * UserService constructor.
     * @param ManagerRegistry $doctrine
     * @param SessionInterface $session
     * @param RouterInterface $router
     */
    public function __construct(ManagerRegistry $doctrine,
        SessionInterface $session,
        RouterInterface $router)
    {
        $this->doctrine = $doctrine;
        $this->session = $session;

        $this->router = $router;
        $this->routeCollection = $router->getRouteCollection();

        $this->anonymous = new User();
        $this->anonymous->setUsername('Anonymous');
        $this->anonymous->setIsAnonymous(true);
        $this->anonymous->addGroup('anonymous');

        $this->setInstance($this);
    }

    /**
     * @return bool
     */
    public function isLoggedin(): bool
    {
        return $this->session->get(self::SESSION_KEY) instanceof User;
    }

    /**
     * @param User $user
     */
    public function login(User $user): void
    {
        $this->session->set(self::SESSION_KEY,$user);
    }

    /**
     *
     */
    public function logout(): void
    {
        $this->session->set(self::SESSION_KEY,null);
    }

    /**
     * @param $password
     * @return bool|string
     */
    public function encryptPassword($password)
    {
        return password_hash($password,PASSWORD_ARGON2_DEFAULT_THREADS);
    }

    /**
     * @return string
     */
    public function createToken(): string
    {
        return uniqid().uniqid().uniqid();
    }

    /**
     * @return User|mixed
     */
    public function user(): User
    {
        if ($this->isLoggedin()) {
            return $this->session->get(self::SESSION_KEY);
        }

        return $this->anonymous;
    }

    /**
     * @param User|null $user
     * @return \App\Entity\User|null
     */
    public function isValidUser(?User $user)
    {
        if ($user) {

            $userEntity = $this->getUserEntityByUser($user,['verified'=>true]);
            if ($userEntity) {
                if (!count($userEntity->getGroups())) {
                    return null;
                }

                if (password_verify($user->getPassword(),$userEntity->getPassword())) {
                    return $userEntity;
                }
            }
        }

        return null;
    }

    /**
     * @param User $user
     * @param array $options
     * @return \BaseApp\BaseappBundle\Entity\User|object|null
     */
    public function getUserEntityByUser(User $user, $options = [])
    {
        return $this->doctrine->getRepository(\BaseApp\BaseappBundle\Entity\User::class)
            ->findOneBy(array_merge(['username'=>$user->getUsername()],$options));
    }

    /**
     * @param string $hash
     * @param array $options
     * @return \BaseApp\BaseappBundle\Entity\User|object|null
     */
    public function getUserEntityByVerification(string $hash, $options = [])
    {
        return $this->doctrine->getRepository(\BaseApp\BaseappBundle\Entity\User::class)
            ->findOneBy(array_merge(['verifyHash'=>$hash],$options));
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->user()->inGroup('admin');
    }

    /**
     * @param $route
     * @return bool
     */
    public function checkAllowRoute($route): bool
    {
        return $this->isAllowed($this->routeCollection->get($route)->getDefault('_allow'));
    }

    /**
     * @param $allow
     * @return bool
     */
    public function isAllowed($allow): bool
    {
        if ($this->isAdmin()) {
            return true;
        }

        /** @var User $user */
        $user = $this->user();

        if (!is_array($allow)) {
            $allow = explode(',',$allow);
        }

        foreach ($allow as $a) {
            if ($user->inGroup($a)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $name
     * @param array $parameters
     * @param int $referenceType
     * @return string
     */
    public function getRoute(string $name, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string
    {
        return $this->router->generate($name,$parameters,$referenceType);
    }
}
