<?php

namespace BaseApp\BaseappBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BaseApp\BaseappBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToOne(targetEntity="BaseApp\BaseappBundle\Entity\User")
     * @ORM\JoinTable(name="user_id")
     */
    private $user;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToOne(targetEntity="BaseApp\BaseappBundle\Entity\User")
     * @ORM\JoinTable(name="user_id")
     */
    private $from;

    /**
     * @ORM\Column(type="boolean", length=255)
     */
    private $unread = true;

    /**
     * @ORM\Column(type="boolean", length=255)
     */
    private $highPrio = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject = '';

    /**
     * @ORM\Column(type="text")
     */
    private $content = '';

    /**
     * @ORM\Column(type="datetime")
     */
    private $sendAt;

    /**
     * Message constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->sendAt = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Message
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Message
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnread()
    {
        return $this->unread;
    }

    /**
     * @param mixed $unread
     * @return Message
     */
    public function setUnread($unread)
    {
        $this->unread = $unread;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHighPrio()
    {
        return $this->highPrio;
    }

    /**
     * @param mixed $highPrio
     * @return Message
     */
    public function setHighPrio($highPrio)
    {
        $this->highPrio = $highPrio;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return Message
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSendAt()
    {
        return $this->sendAt;
    }

    /**
     * @param mixed $sendAt
     * @return Message
     */
    public function setSendAt($sendAt)
    {
        $this->sendAt = $sendAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     * @return Message
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }
}
