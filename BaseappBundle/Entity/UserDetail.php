<?php

namespace BaseApp\BaseappBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BaseApp\BaseappBundle\Repository\UserDetailRepository")
 */
class UserDetail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * One Customer has One Cart.
     * @ORM\OneToOne(targetEntity="User", mappedBy="userdetails")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname = '';

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return UserDetail
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     * @return UserDetail
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }
}
