<?php

namespace BaseApp\BaseappBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BaseApp\BaseappBundle\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Groups", inversedBy="users")
     * @ORM\JoinTable(name="users_groups")
     */
    private $groups;

    /**
     * One Cart has One Customer.
     * @ORM\OneToOne(targetEntity="UserDetail", inversedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userdetails;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password = '';

    /**
     * @ORM\Column(type="boolean", length=255)
     */
    private $verified = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $verifyHash = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token = '';

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     * @return User
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @param mixed $groups
     * @return User
     */
    public function addGroups(Groups $groups)
    {
        $this->groups[] = $groups;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserdetails()
    {
        return $this->userdetails;
    }

    /**
     * @param mixed $userdetails
     * @return User
     */
    public function setUserdetails($userdetails)
    {
        $this->userdetails = $userdetails;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * @param mixed $verified
     * @return User
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerifyHash()
    {
        return $this->verifyHash;
    }

    /**
     * @param mixed $verifyHash
     * @return User
     */
    public function setVerifyHash($verifyHash)
    {
        $this->verifyHash = $verifyHash;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }
}
