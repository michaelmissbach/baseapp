<?php

namespace BaseApp\BaseappBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BaseApp\BaseappBundle\Repository\AlertRepository")
 */
class Alert
{
    const TYPE_SUCCESS = 'success';

    const TYPE_INFO    = 'info';

    const TYPE_WARNING = 'warning';

    const TYPE_ERROR   = 'error';

    const TYPE_LOGINFO   = 'loginfo';


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $content = '';

    /**
     * @ORM\Column(type="string")
     */
    private $type = '';

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * Message constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Alert
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return Alert
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Alert
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Alert
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}
