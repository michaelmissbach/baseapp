<?php

namespace BaseApp\BaseappBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Setting
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $payloadKey = '';

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    protected $payload = '';

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Setting
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayloadkey(): ?string
    {
        return $this->payloadKey;
    }

    /**
     * @param string $payloadKey
     * @return Setting
     */
    public function setPayloadkey(string $payloadKey): Setting
    {
        $this->payloadKey = $payloadKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayload()
    {
        return unserialize($this->payload);
    }

    /**
     * @param mixed $payload
     * @return Setting
     */
    public function setPayload($payload): Setting
    {
        $this->payload = serialize($payload);
        return $this;
    }
}
