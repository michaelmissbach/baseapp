<?php

namespace BaseApp\BaseappBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BaseApp\BaseappBundle\Repository\GroupsRepository")
 */
class Groups
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Many Groups have Many Users.
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groups")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $groupname = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $interngroupname = '';

    /**
     * Group constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Groups
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     * @return Groups
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupname()
    {
        return $this->groupname;
    }

    /**
     * @param mixed $groupname
     * @return Groups
     */
    public function setGroupname($groupname)
    {
        $this->groupname = $groupname;
        $this->interngroupname = $groupname;
        $this->interngroupname = strtolower($this->interngroupname);
        $this->interngroupname = str_replace(' ','',$this->interngroupname);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterngroupname()
    {
        return $this->interngroupname;
    }

    /**
     * @param mixed $interngroupname
     * @return Groups
     */
    public function setInterngroupname($interngroupname)
    {
        $this->interngroupname = $interngroupname;
        return $this;
    }
}
