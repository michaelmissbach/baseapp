<?php

namespace BaseApp\BaseappBundle\Controller;

use BaseApp\BaseappBundle\Form\ForgotPasswordFormType;
use BaseApp\BaseappBundle\Interfaces\IMailSender;
use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ForgotPasswordController
 * @package BaseApp\BaseappBundle\Controller
 */
class ForgotPasswordController extends AbstractController
{    
    /**
     * @var IMailSender
     */
    protected $mailSender;

    /**
     * ForgotPasswordController constructor.
     * @param IMailSender $mailSender
     */
    public function __construct(IMailSender $mailSender)
    {
        $this->mailSender = $mailSender;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $form = $this->createForm(ForgotPasswordFormType::class);

        $form->handleRequest($request);

        $user = null;
        $isError = false;

        if ($form->isSubmitted()) {
            $isError = true;
            if ($form->isValid()) {

                $user = $form->getData();

                if ($userEntity = UserService::$instance->getUserEntityByUser($user)) {

                    $newpassword = uniqid().uniqid();
                    $userEntity->setPassword(UserService::$instance->encryptPassword($newpassword));

                    $this->getDoctrine()->getManager()->persist($userEntity);
                    $this->getDoctrine()->getManager()->flush();

                    $template = $this->renderView('@Baseapp/mails/forgotpassword.html.twig',['user'=>$userEntity,'newpassword'=>$newpassword]);
                    $this->mailSender->sendPasswordForget(
                        $userEntity->getUsername(),
                        $template
                    );
                }

                $this->addFlash('success', 'New password has been created. Please check your emails.');

                return $this->redirect($this->generateUrl('index'));
            }
        }

        return $this->render('@Baseapp/forgotpassword/index.html.twig',
            [
                'form'=>$form->createView(),
                'iserror'=> $form->isSubmitted() && $isError
            ]
        );
    }
}
