<?php

namespace BaseApp\BaseappBundle\Controller;

use BaseApp\BaseappBundle\Entity\Alert;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AlertsController
 * @package BaseApp\BaseappBundle\Controller
 */
class AlertsController extends AbstractController
{   
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        return $this->render('@Baseapp/alerts/index.html.twig',['alerts'=>$this->getDoctrine()->getRepository(Alert::class)->getAll()]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function clear(Request $request)
    {
        $connection = $this->getDoctrine()->getConnection();
        $platform   = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL('alert'));

        return $this->redirect($this->generateUrl('baseapp_alerts'));
    }

    /**
     * @param $params
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getHeader($params)
    {
        $response = [];

        $repo = $this->getDoctrine()->getRepository(Alert::class);
        $count = $repo->getCount();
        $alerts = $repo->getHeader();

        if ((int)$count != (int)$params['count']) {
            $response['count'] = $count;
            $response['template'] = $this->renderView('@Baseapp/alerts/header-inner.html.twig',['alerts'=>$alerts]);
        }

        return $response;
    }
}
