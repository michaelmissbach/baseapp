<?php

namespace BaseApp\BaseappBundle\Controller;

use BaseApp\BaseappBundle\Form\UserPasswordFormType;
use BaseApp\BaseappBundle\Form\UserProfileFormType;
use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProfileController
 * @package BaseApp\BaseappBundle\Controller
 */
class ProfileController extends AbstractController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request)
    {
        $user = UserService::$instance->isValidUser(UserService::$instance->user());

        $form = $this->createForm(UserProfileFormType::class,$user->getUserdetails());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $userDetails = $form->getData();

            $user->setUserdetails($userDetails);

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Profile updated.');
        }

        return $this->render('@Baseapp/profile/update.html.twig',['name'=>$user->getUsername(),'form'=>$form->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updatepassword(Request $request)
    {
        $user = UserService::$instance->isValidUser(UserService::$instance->user());
        $user->setPassword('');

        $form = $this->createForm(UserPasswordFormType::class,$user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();
            $password = UserService::$instance->encryptPassword($user->getPassword());
            $user->setPassword($password);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Profile updated.');
        }

        return $this->render('@Baseapp/profile/update.html.twig',['name'=>$user->getUsername(),'form'=>$form->createView()]);
    }
}
