<?php

namespace BaseApp\BaseappBundle\Controller;

use BaseApp\BaseappBundle\Entity\User;
use BaseApp\BaseappBundle\Form\RegisterFormType;
use BaseApp\BaseappBundle\Interfaces\IMailSender;
use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RegisterController
 * @package BaseApp\BaseappBundle\Controller
 */
class RegisterController extends AbstractController
{
    /**
     * @var IMailSender
     */
    protected $mailSender;

    /**
     * RegisterController constructor.
     * @param IMailSender $mailSender
     */
    public function __construct(IMailSender $mailSender)
    {
        $this->mailSender = $mailSender;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $form = $this->createForm(RegisterFormType::class);

        $form->handleRequest($request);

        $user = null;
        $isError = false;
        $userExists = false;

        if ($form->isSubmitted()) {
            $isError = true;
            if ($form->isValid()) {

                /** @var User $userEntity */
                $userEntity = $form->getData();

                $user = new \BaseApp\BaseappBundle\Model\User();
                $user->setUsername($userEntity->getUsername());

                if (!UserService::$instance->getUserEntityByUser($user)) {

                    $userEntity->setVerifyHash(uniqid().uniqid());
                    $userEntity->setPassword(UserService::$instance->encryptPassword($userEntity->getPassword()));

                    //todo: should this information comes from settings?
                    /*$usergroup = $this->getDoctrine()->getRepository(Groups::class)->findOneBy(['interngroupname'=>'user']);
                    if ($usergroup) {
                        $userEntity->addGroups($usergroup);
                    }*/

                    $this->getDoctrine()->getManager()->persist($userEntity);
                    $this->getDoctrine()->getManager()->flush();

                    $template = $this->renderView('@Baseapp/mails/register.html.twig',['user'=>$userEntity]);
                    $this->mailSender->sendRegister(
                        $userEntity->getUsername(),
                        $template
                    );

                    $this->addFlash('success', 'User created. Please check your emails to verify account.');

                    return $this->redirect($this->generateUrl('index'));
                }

                $userExists = true;
            }
        }

        return $this->render('@Baseapp/register/index.html.twig',
            [
                'form'=>$form->createView(),
                'iserror'=> $form->isSubmitted() && $isError,
                'userExists'=> $userExists,
            ]
        );
    }

    /**
     * @param Request $request
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function verification(Request $request, $hash)
    {
        $userEntity = UserService::$instance->getUserEntityByVerification($hash);
        if (!$userEntity) {
            $this->addFlash('danger', 'User notfound.');
            return $this->redirect($this->generateUrl('index'));
        }

        $userEntity->setVerified(true);

        $this->getDoctrine()->getManager()->persist($userEntity);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'User verified.');
        return $this->redirect($this->generateUrl('index'));
    }
}
