<?php

namespace BaseApp\BaseappBundle\Controller;

use BaseApp\BaseappBundle\Service\AlertService;
use BaseApp\BaseappBundle\Service\AppStarter;
use BaseApp\BaseappBundle\Service\AppService;
use BaseApp\BaseappBundle\Service\JsonApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JsonApiController
 * @package BaseApp\BaseappBundle\Controller
 */
class JsonApiController extends AbstractController
{
    /**
     * @var JsonApiService
     */
    protected $jsonApiService;

    /**
     * JsonApiController constructor.
     * @param JsonApiService $jsonApiService
     */
    public function __construct(JsonApiService $jsonApiService)
    {
        $this->jsonApiService = $jsonApiService;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request,$call)
    {
        $response = "";

        try {

            $response = $this->jsonApiService->run($call,$request->get('params'));


        } catch(\Exception $e) {

            AlertService::$instance->exceptionLog($e);
            return new JsonResponse(['error'=>$e->getMessage()],500);

        }

        return new JsonResponse($response,200);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function stack(Request $request)
    {
        AppService::$instance->disableSetSessionLastExecution();

        try {

            $responses = [];

            $params = json_decode($request->get('params'),true);
            if (!$params || is_array($params) && !count($params)) {
                throw new \Exception('No params.');
            }

            foreach ($params as $param) {
                try {
                    $response = $this->jsonApiService->run($param['command'],$param['data']);
                    $responses[$param['key']] = $response;
                } catch(\Exception $e) {
                    AlertService::$instance->exceptionLog($e);
                    $responses[$param['key']] = ['error'=>true,'message'=>$e->getMessage()];
                }
            }


        } catch(\Exception $e) {

            AlertService::$instance->exceptionLog($e);
            return new JsonResponse(['error'=>$e->getMessage()],500);

        }

        return new JsonResponse($responses,200);
    }
}
