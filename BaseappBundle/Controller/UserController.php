<?php

namespace BaseApp\BaseappBundle\Controller;

use BaseApp\BaseappBundle\Entity\User;
use BaseApp\BaseappBundle\Form\LoginFormType;
use BaseApp\BaseappBundle\Form\UserFormType;
use BaseApp\BaseappBundle\Form\UserGroupFormType;
use BaseApp\BaseappBundle\Form\UserPasswordFormType;
use BaseApp\BaseappBundle\Form\UserProfileFormType;
use BaseApp\BaseappBundle\Service\AppService;
use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @package BaseApp\BaseappBundle\Controller
 */
class UserController extends AbstractController
{
    /**
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        $form = $this->createForm(LoginFormType::class);

        $form->handleRequest($request);

        $user = null;
        $isError = false;

        if ($form->isSubmitted()) {
            $isError = true;
            if ($form->isValid()) {

                $user = $form->getData();

                if ($userEntity = UserService::$instance->isValidUser($user)) {

                    $userEntity->setToken(UserService::$instance->createToken());
                    $this->getDoctrine()->getManager()->persist($userEntity);
                    $this->getDoctrine()->getManager()->flush();

                    $user->setId($userEntity->getId());
                    $user->setToken($userEntity->getToken());

                    foreach ($userEntity->getGroups() as $group) {
                        $user->addGroup($group->getInterngroupname());
                    }
                    UserService::$instance->login($user);
                    AppService::$instance->setSessionLastExecution();

                    if ($origin = AppService::$instance->getRedirectOrigin()) {
                        AppService::$instance->setRedirectOrigin(null);
                        return $this->redirect($origin);
                    }

                    //check if route index exists, otherwise redirect to "/"
                    return $this->redirect($this->generateUrl('index'));

                }
            }
        }

        return $this->render('@Baseapp/user/login.html.twig',
            [
                'form'=>$form->createView(),
                'iserror'=> $form->isSubmitted() && $isError
            ]
        );
    }

    /**
     * @return RedirectResponse
     */
    public function logout()
    {
        if ($userEntity = UserService::$instance->isValidUser(UserService::$instance->user())) {
            $userEntity->setToken('');
            $this->getDoctrine()->getManager()->persist($userEntity);
            $this->getDoctrine()->getManager()->flush();
        }

        UserService::$instance->logout();

        return new RedirectResponse($this->generateUrl('index'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render('@Baseapp/user/index.html.twig',['users'=>$users]);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'User deleted.');

        return $this->redirect($this->generateUrl('baseapp_user_index'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function create()
    {
        $user = new User();
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush($user);

        $this->addFlash('success', 'User created.');

        return $this->redirect($this->generateUrl('baseapp_user_index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request,$id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $form = $this->createForm(UserFormType::class,$user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'User updated.');

            return $this->redirect($this->generateUrl('baseapp_user_index'));

        }

        return $this->render('@Baseapp/user/update.html.twig',['name'=>$user->getUsername(),'form'=>$form->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updatepassword(Request $request,$id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $user->setPassword('');

        $form = $this->createForm(UserPasswordFormType::class,$user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();
            $password = UserService::$instance->encryptPassword($user->getPassword());
            $user->setPassword($password);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'User updated.');

            return $this->redirect($this->generateUrl('baseapp_user_index'));

        }

        return $this->render('@Baseapp/user/update.html.twig',['name'=>$user->getUsername(),'form'=>$form->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updategroup(Request $request,$id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $form = $this->createForm(UserGroupFormType::class,$user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'User updated.');

            return $this->redirect($this->generateUrl('baseapp_user_index'));

        }

        return $this->render('@Baseapp/user/update.html.twig',['name'=>$user->getUsername(),'form'=>$form->createView()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateprofile(Request $request,$id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $form = $this->createForm(UserProfileFormType::class,$user->getUserdetails());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $userDetails = $form->getData();

            $user->setUserdetails($userDetails);

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'User updated.');

            return $this->redirect($this->generateUrl('baseapp_user_index'));

        }

        return $this->render('@Baseapp/user/update.html.twig',['name'=>$user->getUsername(),'form'=>$form->createView()]);
    }
}
