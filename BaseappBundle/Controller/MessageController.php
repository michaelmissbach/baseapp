<?php

namespace BaseApp\BaseappBundle\Controller;

use BaseApp\BaseappBundle\Entity\User;
use BaseApp\BaseappBundle\Model\Message;
use BaseApp\BaseappBundle\Form\MessageFormType;
use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MessageController
 * @package BaseApp\BaseappBundle\Controller
 */
class MessageController extends AbstractController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $messages = $this->getDoctrine()->getRepository(\BaseApp\BaseappBundle\Entity\Message::class)
            ->getAll(UserService::$instance->user()->getId());

        if (count($messages)) {
            $messages[0]->setUnread(false);
            $this->getDoctrine()->getManager()->persist($messages[0]);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render('@Baseapp/message/index.html.twig',['messages'=>$messages]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $message = new Message();

        $form = $this->createForm(MessageFormType::class,$message);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Message $message */
            $message = $form->getData();
            $users = explode(',',$message->getUser());
            $users = array_filter($users);
            $fromEntity = UserService::$instance->getUserEntityByUser(UserService::$instance->user());

            foreach ($users as $user) {
                $userEntity = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username'=>trim($user)]);
                if ($userEntity) {
                    $messageEntity = new \BaseApp\BaseappBundle\Entity\Message();
                    $messageEntity->setSubject($message->getSubject());
                    $messageEntity->setContent($message->getContent());
                    $messageEntity->setHighPrio($message->getHighPrio());
                    $messageEntity->setUser($userEntity);
                    $messageEntity->setFrom($fromEntity);
                    $this->getDoctrine()->getManager()->persist($messageEntity);
                }
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Message sent.');

            return $this->redirect($this->generateUrl('baseapp_message'));

        }

        return $this->render('@Baseapp/message/create.html.twig',['form'=>$form->createView()]);
    }

    /**
     * @param $params
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUnreadMessagesHeader($params)
    {
        $response = [];

        $repo = $this->getDoctrine()->getRepository(\BaseApp\BaseappBundle\Entity\Message::class);
        $count = $repo->getUnreadCount(UserService::$instance->user()->getId());
        $messages = $repo->getUnread(UserService::$instance->user()->getId(),3);

        if ((int)$count != (int)$params['count']) {
            $response['count'] = $count;
            $response['template'] = $this->renderView('@Baseapp/message/partials/header/messages-inner.html.twig',['headermessages'=>$messages]);
        }

        return $response;
    }

    /**
     * @param $params
     * @return array
     */
    public function getContent($params)
    {
        $message = $this->getDoctrine()->getRepository(\BaseApp\BaseappBundle\Entity\Message::class)->find($params['id']);

        if ($message) {
            $message->setUnread(false);
            $this->getDoctrine()->getManager()->persist($message);
            $this->getDoctrine()->getManager()->flush();
            $content = $this->renderView('@Baseapp/message/partials/detail/detail-entry-inner.html.twig',['message'=>$message]);
            return ['content'=>$content];
        }
    }
}
