<?php

namespace BaseApp\BaseappBundle\Controller;

use BaseApp\BaseappBundle\Entity\Setting;
use BaseApp\BaseappBundle\Form\SettingsFormType;
use BaseApp\BaseappBundle\Model\Dynamic;
use BaseApp\BaseappBundle\Service\SettingsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SettingsController
 * @package BaseApp\BaseappBundle\Controller
 */
class SettingsController extends AbstractController
{  
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $data = new Dynamic();

        foreach (SettingsService::$instance->getAll() as $key=>$value) {
            $data->$key = $value;            
        }

        $form = $this->createForm(
            SettingsFormType::class,
            $data,
            ['fields' => SettingsService::$instance->getConfigSettings()]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $repo = $this->getDoctrine()->getRepository(Setting::class);
            foreach ($data->getAll() as $key=>$value) {
                /** @var Setting $entity */
                $entity = $repo->findOneByPayloadKey($key);
                if ($entity) {
                    $entity->setPayload($value);
                } else {
                    $entity = new Setting();
                    $entity->setPayloadkey($key);
                    $entity->setPayload($value);
                }
                $manager->persist($entity);
            }
            $manager->flush();
        }

        return $this->render('@Baseapp/settings/edit.html.twig',
            [
                'form'=>$form->createView(),
                'groups' => SettingsService::$instance->getConfigSettingGroups(),
                'fields' => SettingsService::$instance->getConfigSettings(),
            ]
        );
    }
}
