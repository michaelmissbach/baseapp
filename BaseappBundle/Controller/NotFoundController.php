<?php

namespace BaseApp\BaseappBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class NotFoundController
 * @package App\Controller
 */
class NotFoundController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show()
    {
        return $this->render('@Baseapp/notfound/show.html.twig');
    }
}
