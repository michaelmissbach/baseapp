<?php

namespace BaseApp\BaseappBundle\Controller;

use BaseApp\BaseappBundle\Entity\Groups;
use BaseApp\BaseappBundle\Form\GroupFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GroupController
 * @package BaseApp\BaseappBundle\Controller
 */
class GroupController extends AbstractController
{    
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $groups = $this->getDoctrine()->getRepository(Groups::class)->findAll();
        return $this->render('@Baseapp/group/index.html.twig',['groups'=>$groups]);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete($id)
    {
        $groups = $this->getDoctrine()->getRepository(Groups::class)->find($id);
        $this->getDoctrine()->getManager()->remove($groups);
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success', 'Group deleted.');
        return $this->redirect($this->generateUrl('baseapp_group_index'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function create()
    {
        $groups = new Groups();
        $this->getDoctrine()->getManager()->persist($groups);
        $this->getDoctrine()->getManager()->flush($groups);
        $this->addFlash('success', 'Group created.');

        return $this->redirect($this->generateUrl('baseapp_group_index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request,$id)
    {
        $group = $this->getDoctrine()->getRepository(Groups::class)->find($id);

        $form = $this->createForm(GroupFormType::class,$group);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $group = $form->getData();
            $this->getDoctrine()->getManager()->persist($group);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Group updated.');

            return $this->redirect($this->generateUrl('baseapp_group_index'));
        }

        return $this->render('@Baseapp/group/update.html.twig',['form'=>$form->createView()]);
    }
}
