<?php

namespace BaseApp\BaseappBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class WebsocketServerController
 * @package BaseApp\BaseappBundle\Controller
 */
class WebsocketServerController extends AbstractController
{
    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * WebsocketServerController constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {

        return $this->render('@Baseapp/websocket/index.html.twig',['ports'=>[1500,1501,1502,1503]]);
    }

    /**
     * @param $params
     */
    public function startserver($params)
    {
        $path = $this->get('kernel')->getProjectDir();
        exec(sprintf('php %s/bin/console baseapp:socket:run %s  > /dev/null 2>&1 &',$path,$params['port']),$output);
    }
}
