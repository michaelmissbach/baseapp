<?php

namespace BaseApp\BaseappBundle;

use BaseApp\BaseappBundle\DependencyInjection\Compiler\CronjobPass;
use BaseApp\BaseappBundle\DependencyInjection\Compiler\GuiBuilderPass;
use BaseApp\BaseappBundle\DependencyInjection\Compiler\InstallPass;
use BaseApp\BaseappBundle\DependencyInjection\Compiler\ServerHandlerPass;
use BaseApp\BaseappBundle\DependencyInjection\Compiler\SettingsPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class BaseappBundle
 * @package BaseApp\BaseappBundle
 */
class BaseappBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new SettingsPass());
        $container->addCompilerPass(new GuiBuilderPass());
        $container->addCompilerPass(new InstallPass(),PassConfig::TYPE_REMOVE);
        $container->addCompilerPass(new CronjobPass(),PassConfig::TYPE_OPTIMIZE);
        $container->addCompilerPass(new ServerHandlerPass(),PassConfig::TYPE_OPTIMIZE);
    }
}
