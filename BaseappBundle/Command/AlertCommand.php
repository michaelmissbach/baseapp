<?php

namespace BaseApp\BaseappBundle\Command;

use BaseApp\BaseappBundle\Entity\Alert;
use BaseApp\BaseappBundle\Entity\Groups;
use BaseApp\BaseappBundle\Entity\User;
use BaseApp\BaseappBundle\Service\AlertService;
use BaseApp\BaseappBundle\Service\AppStarter;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AlertCommand
 * @package BaseApp\BaseappBundle\Command
 */
class AlertCommand extends Command
{
    const ARGUMENT_TYPE = 'type';
    const ARGUMENT_MESSAGE = 'message';

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * @var AppStarter
     */
    protected $appStarter;

    /**
     * AlertCommand constructor.
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine, AppStarter $appStarter)
    {
        $this->doctrine = $doctrine;
        $this->appStarter = $appStarter;
        parent::__construct();
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'baseapp:create:alert';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Scan the network and log existing devices.')
            ->addArgument(self::ARGUMENT_TYPE,InputArgument::REQUIRED)
            ->addArgument(self::ARGUMENT_MESSAGE,InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appStarter->run();

        $type = $input->getArgument(self::ARGUMENT_TYPE);
        $message = $input->getArgument(self::ARGUMENT_MESSAGE);

        if(!AlertService::$instance->isAllowedType($type)) {
            $output->writeln(sprintf('Unknown type "%s".',$type));
            return 1;
        }

        $alert = $this->doctrine->getRepository(Alert::class)->createNew($type,$message);
        $this->doctrine->getManager()->persist($alert);
        $this->doctrine->getManager()->flush();

        $output->writeln(sprintf('Done.'));

        return 0;
    }
}
