<?php

namespace BaseApp\BaseappBundle\Command;

use BaseApp\BaseappBundle\Entity\Groups;
use BaseApp\BaseappBundle\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UserCommand
 * @package BaseApp\BaseappBundle\Command
 */
class UserCommand extends Command
{
    const ARGUMENT_USERNAME = 'username';
    const ARGUMENT_PASSWORD = 'password';
    const ARGUMENT_GROUPS = 'groups';

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * UserCommand constructor.
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
        parent::__construct();
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'baseapp:create:user';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Scan the network and log existing devices.')
            ->addArgument(self::ARGUMENT_USERNAME,InputArgument::REQUIRED)
            ->addArgument(self::ARGUMENT_PASSWORD,InputArgument::REQUIRED)
            ->addArgument(self::ARGUMENT_GROUPS,InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument(self::ARGUMENT_USERNAME);
        $password = $input->getArgument(self::ARGUMENT_PASSWORD);
        $groups = $input->getArgument(self::ARGUMENT_GROUPS);

        $user = $this->doctrine->getRepository(User::class)->findOneBy(['username'=>$username]);

        if (!$user) {
            $user = new User();
            $user->setUsername($username);
            $user->setVerified(true);
        } else {
            $user->setVerified(true);
            $output->writeln(sprintf('User %s already exists. Modify current.',$username));
        }

        $user->setPassword(password_hash($password,PASSWORD_DEFAULT));
        $user->setGroups([]);

        $groups = explode(',',$groups);

        if (is_array($groups)) {
            foreach ($groups as $groupname) {
                $groupEntity = $this->doctrine->getRepository(Groups::class)->findOneBy(['interngroupname'=>$groupname]);
                if ($groupEntity) {
                    $user->addGroups($groupEntity);
                }
            }
        }

        $this->doctrine->getManager()->persist($user);
        $this->doctrine->getManager()->flush();

        $output->writeln(sprintf('Done.',$username));

        return 0;
    }
}
