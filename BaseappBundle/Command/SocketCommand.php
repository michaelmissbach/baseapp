<?php

namespace BaseApp\BaseappBundle\Command;

use BaseApp\BaseappBundle\Server\Log;
use BaseApp\BaseappBundle\Server\WebsocketServer;
use BaseApp\BaseappBundle\Service\AlertService;
use Doctrine\Persistence\ManagerRegistry;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class UserCommand
 * @package BaseApp\BaseappBundle\Command
 */
class SocketCommand extends Command
{
    const ARGUMENT_PORT = 'port';
    const ARGUMENT_ADDRESS = 'address';

    /**
     * @var
     */
    protected $IOServer;

    /**
     * @var WebsocketServer
     */
    protected $server;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * SocketCommand constructor.
     * @param WebsocketServer $server
     * @param ManagerRegistry $doctrine
     * @param KernelInterface $kernel
     * @param AlertService $kernel
     */
    public function __construct(
        WebsocketServer $server,
        ManagerRegistry $doctrine,
        KernelInterface $kernel,
        AlertService $alertService
        )
    {
        ini_set('max_execution_time',60*60*12);

        $alertService->run();

        Log::$path = $kernel->getLogDir().'/';

        $this->server = $server;
        $this->doctrine = $doctrine;

        $doctrine->getManager()->getConnection()->getConfiguration()->setSQLLogger(null);
        parent::__construct();
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'baseapp:socket:run';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Scan the network and log existing devices.')
            ->addArgument(self::ARGUMENT_PORT,InputArgument::REQUIRED)
            ->addArgument(self::ARGUMENT_ADDRESS,InputArgument::OPTIONAL)
            /*->addArgument(self::ARGUMENT_PASSWORD,InputArgument::REQUIRED)
            ->addArgument(self::ARGUMENT_GROUPS,InputArgument::REQUIRED)*/
            ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $port = $input->getArgument(self::ARGUMENT_PORT);
        $address = $input->getArgument(self::ARGUMENT_ADDRESS) ?: '0.0.0.0';

        Log::setPort($port);
        Log::clear();

        Log::log(sprintf('Server started (Address: %s, Port: %s).',$address,$port));

        try {

            $this->IOServer =
                IoServer::factory(
                    new HttpServer(
                        new WsServer(
                            $this->server
                        )
                    ),
                    $port,
                    $address
                );

            $this->server->setStopCallback([$this,'stopCallback']);

            $s = $this->server;
            $this->IOServer->loop->addPeriodicTimer(10,function() use($s){
                $s->onTick();
            });
            $this->IOServer->loop->addPeriodicTimer(300,function() use($s){
                $s->onLongTick();
            });


            $this->IOServer->run();
        }
        catch(\Exception $e) {
            AlertService::$instance->externExceptionLog($this->doctrine,$e);
            Log::log($e->getMessage());
        }

        Log::log('Server stopped.');
        Log::write();

        return 0;
    }

    /**
     *
     */
    public function stopCallback()
    {
        $this->IOServer->loop->stop();
    }
}
