<?php

namespace BaseApp\BaseappBundle\Command;

use BaseApp\BaseappBundle\Cronjob\CronjobContainer;
use BaseApp\BaseappBundle\Entity\Groups;
use BaseApp\BaseappBundle\Service\AlertService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CronjobCommand
 * @package BaseApp\BaseappBundle\Command
 */
class CronjobCommand extends Command
{
    /**
     * @var CronjobContainer
     */
    protected $cronjobContainer;

    /**
     * CronjobCommand constructor.
     * @param CronjobContainer $cronjobContainer
     */
    public function __construct(CronjobContainer $cronjobContainer, AlertService $alertService)
    {
        $alertService->run();
        $this->cronjobContainer = $cronjobContainer;
        parent::__construct();
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'baseapp:cronjob:run';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('')
            //->addArgument('group',InputArgument::REQUIRED)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Cronjobs start.');
        $this->cronjobContainer->run();
        $output->writeln('Cronjobs end.');

        return 0;
    }
}
