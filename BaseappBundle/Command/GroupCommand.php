<?php

namespace BaseApp\BaseappBundle\Command;

use BaseApp\BaseappBundle\Entity\Groups;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GroupCommand
 * @package BaseApp\BaseappBundle\Command
 */
class GroupCommand extends Command
{
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * GroupCommand constructor.
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
        parent::__construct();
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'baseapp:create:group';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Scan the network and log existing devices.')
            ->addArgument('group',InputArgument::REQUIRED)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $groupname = $input->getArgument('group');

        $group = $this->doctrine->getRepository(Groups::class)->findOneBy(['interngroupname'=>$groupname]);

        if ($group) {
            $output->writeln(sprintf('Group %s already exists.',$groupname));
            return 1;
        }

        $group = new Groups();
        $group->setGroupname($groupname);

        $this->doctrine->getManager()->persist($group);
        $this->doctrine->getManager()->flush();

        $output->writeln(sprintf('Group %s created.',$groupname));

        return 0;
    }
}
