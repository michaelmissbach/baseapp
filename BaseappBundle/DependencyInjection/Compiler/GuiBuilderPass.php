<?php

namespace BaseApp\BaseappBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class GuiBuilderPass
 * @package BaseApp\BaseappBundle\DependencyInjection\Compiler
 */
class GuiBuilderPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('baseapp_guibuilder')) {
            return;
        }

        $definition = $container->findDefinition('baseapp_guibuilder');

        $taggedServices = $container->findTaggedServiceIds('baseapp.gui');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('add', array(new Reference($id)));
        }
    }
}
