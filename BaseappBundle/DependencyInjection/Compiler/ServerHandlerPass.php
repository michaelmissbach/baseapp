<?php

namespace BaseApp\BaseappBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ServerHandlerPass
 * @package BaseApp\BaseappBundle\DependencyInjection\Compiler
 */
class ServerHandlerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('baseapp.websocket.server')) {
            return;
        }

        $definition = $container->findDefinition('baseapp.websocket.server');

        $taggedServices = $container->findTaggedServiceIds('baseapp.websocket.handler');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addHandler', array(new Reference($id)));
        }
    }
}
