<?php

namespace BaseApp\BaseappBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class InstallPass
 * @package BaseApp\BaseappBundle\DependencyInjection\Compiler
 */
class InstallPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {

    }
}
