<?php

namespace BaseApp\BaseappBundle\DependencyInjection\Compiler;

use BaseApp\BaseappBundle\Interfaces\IAppSettingGroups;
use BaseApp\BaseappBundle\Interfaces\IAppSettings;
use BaseApp\BaseappBundle\Service\SettingsService;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class SettingsPass
 * @package BaseApp\BaseappBundle\DependencyInjection\Compiler
 */
class SettingsPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('baseapp_appsettings');

        $definition->addMethodCall('registerSettingGroupsArray', array($this->getAppSettingGroups()));
        $definition->addMethodCall('registerSettingsArray', array($this->getAppSettings()));


        $taggedServices = $container->findTaggedServiceIds('baseapp.settings');
        foreach ($taggedServices as $id => $tags) {

            if (!is_subclass_of($id, IAppSettings::class)) {
                throw new \Exception(sprintf('Class %s must be instanceof IAppSettings',$id));
            }

            $instance = new Reference($id);
            if (is_subclass_of($id, IAppSettingGroups::class)) {
                $definition->addMethodCall('registerSettingGroups', array($instance));
            }

            $definition->addMethodCall('registerSettings', array($instance));
        }
    }

    /**
     * @return array
     */
    protected function getAppSettingGroups(): array
    {
        return [
            [
                SettingsService::SETTINGS_GROUP_KEY => 'system',
                SettingsService::SETTINGS_GROUP_NAME => 'System'
            ]
        ];
    }

    /**
     * @return array
     */
    protected function getAppSettings(): array
    {
        return [
            /*[
                SettingsService::SETTINGS_KEY => 'test',
                SettingsService::SETTINGS_GROUP_KEY => 'system',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'einfaches label'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'user'
            ]*/
        ];
    }
}
