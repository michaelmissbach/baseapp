<?php

namespace BaseApp\BaseappBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class CronjobPass
 * @package BaseApp\BaseappBundle\DependencyInjection\Compiler
 */
class CronjobPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('baseapp.cronjob.container')) {
            return;
        }

        $definition = $container->findDefinition('baseapp.cronjob.container');

        $taggedServices = $container->findTaggedServiceIds('baseapp.cronjob');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addTask', array(new Reference($id)));
        }
    }
}
