$(document).ready(function() {
    Baseapp.addToIntervalCallStack(
        'baseapp_appstate:isLoggedout',
        {},
        function(response){
            if (response) {
                Baseapp.Modal.show(
                    'Logged out',
                    'You have been logged out due to inactivity.',
                    'Ok',
                    document.location.href
                );
            }
        }
    )
});