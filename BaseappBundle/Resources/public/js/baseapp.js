const Baseapp = {
    intervalcallstack: [],
    callstack: [],
    interval: 30,
    apicall: function (command, data = {}, callback = null) {
        if (Baseapp.isFunction(data)) {
            data = data();
        }
        $.ajax(
            {
                url: BaseappSingleJsonUri + "/" + command,
                data: {'params': data},
                method: "POST"
            }
        ).done(function (e) {
            if (Baseapp.isFunction(callback)) {
                callback(e);
            }
        });
    },
    apistackcall: function(stack) {
        let callParams = [];
        for(let call in stack) {
            let callParam = Baseapp.generateStackElement(stack[call].command,stack[call].data,stack[call].callback);

            if (Baseapp.isFunction(callParam.data)) {
                callParam.data = callParam.data();
            }
            callParams.push(callParam);

        };
        $.ajax(
            {
                url: BaseappStackJsonUri,
                data: {'params': JSON.stringify(callParams)},
                type: "POST"
            }
        ).done(function (e) {
            for(let call in callParams) {
                if (Baseapp.isFunction(callParams[call].callback)) {
                    callParams[call].callback(e[callParams[call].key]);
                }
            }
        });
    },
    reload: function() {
        window.location.reload();
    },
    isFunction: function(functionToCheck) {
        return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
    },
    addToCallStack: function(command, data = {}, callback = null) {
        Baseapp.callstack.push(Baseapp.generateStackElement(command,data,callback));
    },
    addToIntervalCallStack: function(command, data = {}, callback = null) {
        Baseapp.intervalcallstack.push(Baseapp.generateStackElement(command,data,callback));
    },
    clearStack: function() {
        Baseapp.callstack = [];
    },
    uniqid: function uniqid() {
        /*var ts=String(new Date().getTime()), i = 0, out = '';
        for(i=0;i<ts.length;i+=2) {
            out+=Number(ts.substr(i, 2)).toString(36);
        }
        return ('d'+out);*/
        let n = Math.floor(Math.random() * 11);
        let k = Math.floor(Math.random() * 1000000);
        return String.fromCharCode(n) + k;
    },
    generateStackElement: function(command, data = {}, callback = null) {
        let call = Object.create(Baseapp.callStackElement);
        call.init(command,data,callback);
        return call;
    },
    callStackElement: {
        key: '',
        command:'',
        data: '',
        callback: null,
        init: function(command, data, callback) {
            this.key = Baseapp.uniqid();
            this.command = command;
            this.data = data;
            this.callback = callback;
        }
    },
    run: function() {
        window.setTimeout(function(){
            if (Baseapp.intervalcallstack.length > 0) {
                Baseapp.apistackcall(Baseapp.intervalcallstack);
            }
        },1000);
        window.setInterval(function(){
            if (Baseapp.intervalcallstack.length > 0) {
                Baseapp.apistackcall(Baseapp.intervalcallstack);
            }
        },Baseapp.interval * 1000);
    },
    Messages: {
        getCurrentUnreadMessagesFromHeader: function() {
            let count = parseInt($('#baseapp-message-header-count').html());
            count = isNaN(count) ? 0 : count;
            return {'count':count};
        },
        setCurrentUnreadMessagesFromHeader: function(count) {
            $('#baseapp-message-header-count').html(count);
        },
        setUnreadMessagesContentFromHeader: function(content) {
            $('#baseapp-message-header-content').html(content);
        },
        updateHeader: function(response) {
            if (response.count) {
                Baseapp.Messages.setCurrentUnreadMessagesFromHeader(response.count);
            }
            if (response.template) {
                Baseapp.Messages.setUnreadMessagesContentFromHeader(response.template);
            }
        },
        updateContent: function(response) {
            if (response.content) {
                $('#messages-detail-inner').html(response.content);
            }
        },
        run: function() {
            $('tr[data-function="messages-list-entry"]').on('click',function(){
                $('tr[data-function="messages-list-entry"]').each(function(i,e){
                    $(this).attr('data-active','false');
                });
                $(this).attr('data-active','true');
                Baseapp.apicall('baseapp_message_controller:getContent',{'id':$(this).data('detail-id')},Baseapp.Messages.updateContent);
            });
        }
    },
    Alerts: {
        getCurrentAlertsFromHeader: function() {
            let count = parseInt($('#baseapp-alerts-counter').html());
            count = isNaN(count) ? 0 : count;
            return {'count':count};
        },
        setCurrentAlertsFromHeader: function(count) {
            $('#baseapp-alerts-counter').html(count);
        },
        setAlertsContentFromHeader: function(content) {
            $('#baseapp-alerts-content').html(content);
        },
        updateHeader: function(response) {
            if (response.count) {
                Baseapp.Alerts.setCurrentAlertsFromHeader(response.count);
            }
            if (response.template) {
                Baseapp.Alerts.setAlertsContentFromHeader(response.template);
            }
        },
    },
    Modal: {
        show: function(title,body,button,href) {
            $('#BaseappModal .modal-title').html(title);
            $('#BaseappModal .modal-body').html(body);
            $('#BaseappModal .btn-primary').html(button);
            $('#BaseappModal .btn-primary').attr('href',href);
            $('#BaseappModal').modal();
        },
    },
    Websocket: {
        run: function(){
            $('a').on('click',function(e){
                Baseapp.Websocket.closeAll();
            });
        },
        sockets:[],
        create: function(port){
            let i = Object.create(Baseapp.Websocket.instance);
            i.init(i,port);
            this.sockets[port] = i;
            return i;
        },
        closeAll: function(){
            for (let s in this.sockets) {
                if (sockets.hasOwnProperty(s)) {
                    sockets[s].close();
                }
            }
        },
        instance: {
            instance: null,
            port:null,
            connection : null,
            listeners: [],
            init: function(instance,port){
                this.instance = instance;
                this.port = port;
                this.connection = null;
                this.listeners = [];
            },
            open: function() {
                var that=this;
                if (Baseapp.Websocket.connection != null) {
                    console.log('Connection already open.');
                    return;
                }
                //overwrite it:
                this.instance.listeners['message'] = console.log;
                this.instance.listeners['health'] = console.log;
                this.instance.listeners['auth'] = console.log;
                this.instance.listeners['log'] = console.log;

                this.instance.connection = new WebSocket('ws://'+BaseappServerHost+":"+this.port);

                this.instance.connection.onopen = function(e) {
                    console.log("Connection established!");
                };
                this.instance.connection.onerror = function(e) {
                    console.error(e.message);
                };
                this.instance.connection.onmessage = function(e) {
                    let message = JSON.parse(e.data);
                    if (that.instance.listeners[message.command]) {
                        that.instance.listeners[message.command](message.params);
                    } else {
                        if (message.command === "") {
                            console.error('No command specified.');
                            console.log(message.params);
                        } else {
                            console.error('No listener for command "'+message.command+'' + '".');
                        }
                    }
                };
                this.instance.connection.onclose = function(e) {
                    that.instance.connection = null;
                    console.log('Connection close');
                };
            },
            close: function() {
                this.instance.connection.close();
                this.instance.connection = null;
            },
            clear: function() {
                this.instance.listeners = [];
            },
            addListener: function(command,callback) {
                this.instance.listeners[command] = callback;
            },
            send: function(command,params = []) {
                if (this.instance.connection == null) {
                    console.log('Not connected to websocket.');
                    return;
                }

                this.instance.connection.send(JSON.stringify({'command':command,'params':params}));
            }
        },

    }
};

Baseapp.run();

