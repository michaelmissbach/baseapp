<?php

namespace BaseApp\BaseappBundle\Cronjob;

use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Interface ICronjob
 * @package BaseApp\BaseappBundle\Cronjob
 */
interface ICronjob
{    
    /**
     * getInterval
     *
     * @return int
     */
    public function getSecondInterval(): int;

    /**
     * @param ParameterBag $request
     * @param ParameterBag $parameterBag
     */
    public function run(ParameterBag $request,ParameterBag $parameterBag): void;
}
