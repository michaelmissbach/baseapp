<?php

namespace BaseApp\BaseappBundle\Cronjob;

use Doctrine\Persistence\ManagerRegistry;
use BaseApp\BaseappBundle\Service\AppStarter;
use BaseApp\BaseappBundle\Service\AlertService;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class CronjobContainer
 * @package BaseApp\BaseappBundle\Cronjob
 */
class CronjobContainer
{
    const LOCK_FILE = 'baseapp_cronjob.lock';

    const PAYLOAD_FILE = 'baseapp_cronjob_payload.ser';

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * @var string
     */
    protected $cacheDir;
        
    /**
     * kernel
     *
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var []
     */
    protected $container;

    /**
     * @var AppStarter
     */
    protected $appStarter;
    
    /**
     * executions
     *
     * @var array
     */
    protected $payload = [];
        
    /**
     * __construct
     *
     * @param  mixed $kernel
     * @param  mixed $doctrine
     * @param  mixed $appStarter
     * @return void
     */
    public function __construct(KernelInterface $kernel, ManagerRegistry $doctrine, AppStarter $appStarter)
    {
        $this->doctrine = $doctrine;
        $this->kernel = $kernel;

        $this->appStarter = $appStarter;

        $this->cacheDir = $kernel->getCacheDir();
    }

    /**
     * @param ICronjob $cronjob
     */
    public function addTask(ICronjob $cronjob)
    {
        $this->container[] = $cronjob;
    }

    /**
     *
     */
    public function run()
    {
        if ($this->lockFileExists()) {
            echo 'BaseApp Cronjob locked.'.PHP_EOL;
            return;
        }

        $this->appStarter->run();

        $this->setLockFile();
        $this->loadPayload();
        
        $request = new ParameterBag();
        $dateNow = new \DateTime('now');
        $request->set('now',$dateNow);

        /** @var ICronjob $task */
        foreach($this->container as $task) {
            try {

                $hasToRun = false;

                $className = get_class($task);

                if (!array_key_exists($className,$this->payload)) {
                    $this->payload[$className] = ['payload'=>[],'lastExecution'=>null];
                }

                if ($this->payload[$className]['lastExecution'] instanceof \DateTime) {
                    $lastDate = $this->payload[$className]['lastExecution'];
                    
                    $dateToCheck = clone $lastDate;
                    $dateToCheck->add(new \DateInterval(sprintf('PT%sS',$task->getSecondInterval())));
                    $hasToRun = $dateNow >= $dateToCheck;
                    
                } else {
                    $hasToRun = true;
                    $lastDate = new \DateTime();
                }

                if ($hasToRun) {
                    $parameterBag = new ParameterBag($this->payload[$className]['payload']);
                    
                    echo sprintf('Executes %s.%s',$className,PHP_EOL);
                    $request->set('last',$lastDate);
                    
                    try {
                        $task->run($request,$parameterBag);
                    } catch(\Exception | \Throwable $e) {
                        AlertService::$instance->externExceptionLog($this->doctrine,$e);
                    }

                    $nextDate = clone $dateNow;
                    $nextDate->add(new \DateInterval('PT1S'));
                    $this->payload[$className]['lastExecution'] = $nextDate;
                    $this->payload[$className]['payload'] = $parameterBag->all();
                }

            } catch(\Exception | \Throwable $e) {
                AlertService::$instance->externExceptionLog($this->doctrine,$e);
            }
        }

        $this->savePayload();
        $this->removeLockFile();
    }
    
    /**
     * getLockfilePath
     *
     * @return string
     */
    protected function getLockfilePath(): string
    {
        return sprintf('%s/%s',$this->cacheDir,self::LOCK_FILE);
    }

    /**
     * getLockfilePath
     *
     * @return string
     */
    protected function getPayloadFilePath(): string
    {
        return sprintf('%s/%s',$this->cacheDir,self::PAYLOAD_FILE);
    }
    
    /**
     * setLockFile
     *
     * @return void
     */
    protected function setLockFile(): void
    {
        file_put_contents($this->getLockfilePath(),'');
    }

    /**
     * setLockFile
     *
     * @return void
     */
    protected function removeLockFile(): void
    {
        @unlink($this->getLockfilePath());
    }
    
    /**
     * lockFileExists
     *
     * @return bool
     */
    protected function lockFileExists(): bool
    {
        return file_exists($this->getLockfilePath());
    }
    
        
    /**
     * loadPayload
     *
     * @return void
     */
    protected function loadPayload(): void
    {
        if (file_exists($this->getPayloadFilePath())) {
            $this->payload = unserialize(file_get_contents($this->getPayloadFilePath()));
        }
    }
    
    /**
     * savePayload
     *
     * @return void
     */
    protected function savePayload(): void
    {
        file_put_contents($this->getPayloadFilePath(),serialize($this->payload));
    }
}
