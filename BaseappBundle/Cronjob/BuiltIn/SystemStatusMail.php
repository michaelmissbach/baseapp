<?php

namespace BaseApp\BaseappBundle\Cronjob\BuiltIn;

use BaseApp\BaseappBundle\Cronjob\ICronjob;
use BaseApp\BaseappBundle\Entity\Alert;
use BaseApp\BaseappBundle\Interfaces\IAppSettings;
use BaseApp\BaseappBundle\Interfaces\IMailSender;
use BaseApp\BaseappBundle\Service\SettingsService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class SystemStatusMail
 * @package BaseApp\BaseappBundle\Cronjob\BuiltIn
 */
class SystemStatusMail implements ICronjob, IAppSettings
{
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * @var IMailSender
     */
    protected $mailer;

    /**
     * SystemStatusMail constructor.
     * @param ManagerRegistry $managerRegistry
     * @param IMailSender $mailer
     */
    public function __construct(ManagerRegistry $managerRegistry, IMailSender $mailer)
    {
        $this->doctrine = $managerRegistry;
        $this->mailer = $mailer;
    }


    /**
     * @return array[]
     */
    public function getAppSettings(): array
    {
        $hours = [];
        for ($i=0;$i<24;$i++){
            if (strlen($i)<2) {
                $hours[sprintf('0%s',$i)] = sprintf('0%s',$i);
            } else {
                $hours[(string)$i] = (string)$i;
            }
        }

        $minutes = [];
        for ($i=0;$i<60;$i++){
            if (strlen($i)<2) {
                $minutes[sprintf('0%s',$i)] = sprintf('0%s',$i);
            } else {
                $minutes[(string)$i] = (string)$i;
            }
        }

        return [
            [
                SettingsService::SETTINGS_KEY => 'system_status_mail_active',
                SettingsService::SETTINGS_GROUP_KEY => 'system',
                SettingsService::SETTINGS_FORM_TYPE => CheckboxType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'System status mail active',
                    'required' => false
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ],
            [
                SettingsService::SETTINGS_KEY => 'system_status_mail_hour',
                SettingsService::SETTINGS_GROUP_KEY => 'system',
                SettingsService::SETTINGS_FORM_TYPE => ChoiceType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'System status mail send hour',
                    'empty_data' => '',
                    'required' => false,
                    'choices' => $hours
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ],
            [
                SettingsService::SETTINGS_KEY => 'system_status_mail_minute',
                SettingsService::SETTINGS_GROUP_KEY => 'system',
                SettingsService::SETTINGS_FORM_TYPE => ChoiceType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'System status mail send minutue',
                    'empty_data' => '',
                    'required' => false,
                    'choices' => $minutes
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ],
            [
                SettingsService::SETTINGS_KEY => 'system_status_mail_receiver',
                SettingsService::SETTINGS_GROUP_KEY => 'system',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'System status mail receiver email address',
                    'required' => false
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ]
        ];
    }

    /**
     * @return int
     */
    public function getSecondInterval(): int
    {
        return 60;
    }

    /**
     * @param ParameterBag $request
     * @param ParameterBag $parameterBag
     */
    public function run(ParameterBag $request, ParameterBag $parameterBag): void
    {
        $active = (bool)SettingsService::$instance->get(SettingsService::createSemanticKey('system','system_status_mail_active'),false);

        if (!$active) {
            return;
        }

        $hour = (int)SettingsService::$instance->get(SettingsService::createSemanticKey('system','system_status_mail_hour'),0);
        $minute = (int)SettingsService::$instance->get(SettingsService::createSemanticKey('system','system_status_mail_minute'),0);
        $receiver = SettingsService::$instance->get(SettingsService::createSemanticKey('system','system_status_mail_receiver'),null);

        $today = new \DateTime('now');
        $today->setTime($hour,$minute);

        if (!($today >= $request->get('last') && $today <= $request->get('now'))) {
            return;
        }

        $entries = $this->doctrine->getRepository(Alert::class)->getAll();

        $data = [];
        /** @var Alert $entry */
        foreach ($entries as $entry) {
            $data[] = sprintf(
                '"%s" at %s: %s',
                ucfirst($entry['type']),
                $entry['createdAt']->format('d.m.Y H:i'),
                $entry['content']
            );
        }
        if (!count($data)) {
            $data[] = "No events occured.";
        }

        $this->mailer->send(
            [$receiver],
            ['test@test.de'],//todo: add from sender mail setting!
            'Baseapp system log.',//todo: add naming in settings?
            implode('<br>',$data)
        );

        $connection = $this->doctrine->getConnection();
        $platform   = $connection->getDatabasePlatform();
        $connection->executeUpdate($platform->getTruncateTableSQL('alert'));
    }
}
