<?php

namespace BaseApp\BaseappBundle\Server;

use BaseApp\BaseappBundle\Server\Handler\IHandler;
use Doctrine\Persistence\ManagerRegistry;
use Monolog\Handler\PHPConsoleHandler;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class WebsocketServer
 * @package BaseApp\BaseappBundle\Server
 * http://socketo.me/docs/hello-world
 */
class WebsocketServer implements MessageComponentInterface
{
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * @var ParameterBag
     */
    protected $clients;

    /**
     * @var ParameterBag
     */
    protected $handler;

    /**
     * @var Callback
     */
    protected $stopCallBack;

    /**
     * WebsocketServer constructor.
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->clients = new ParameterBag();
        $this->handler = new ParameterBag();

        gc_enable();
    }

    /**
     * @param $stopCallBack
     */
    public function setStopCallback($stopCallBack)
    {
        $this->stopCallBack = $stopCallBack;
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $connection = new Connection($conn);
        $this->clients->set($conn->resourceId,$connection);

        Log::log("New connection! ({$conn->resourceId})");
    }

    /**
     * @param $command
     * @param IHandler $handler
     */
    public function addHandler(IHandler $handler)
    {
        $this->handler->set($handler->getCommand(),$handler);
    }

    /**
     *
     */
    public function onTick()
    {
        Log::write();
    }

    /**
     *
     */
    public function onLongTick()
    {
        //https://github.com/php-pm/php-pm/issues/391
        $this->doctrine->getConnection()->ping();
    }

    /**
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        $msg = json_decode($msg,true);

        /** @var Connection $from */
        $from = $this->clients->get($from->resourceId);

        Log::log(sprintf('Command "%s" from "%s".',$msg['command'],$from->getConnection()->resourceId));

        /** @var IHandler $handler */
        $handler = $this->handler->get($msg['command']);

        if (!$handler) {
            Log::log(sprintf('No handler for "%s".',$msg['command']));
            $message = json_encode(['command'=>'','params'=>sprintf('No handler for "%s".',$msg['command'])]);
            $from->getConnection()->send($message);
            return;
        }
        try {

            /** @var Response $response */
            $response = $handler->execute($from,$msg['params'],$this->clients);
            $message = json_encode(['command'=>$response->getCommand(),'params'=>$response->getMessage()]);

        } catch (\Exception $e) {
            $message = json_encode(['command'=>'message','params'=>$e->getMessage().'/'.$e->getFile().'/ Line: '.$e->getLine()]);
            if (!$response) {
                $response = new Response(IReceiver::MYSELF);
            }
            $response->setReceiver(IReceiver::MYSELF);
        }

        $numRecv = 0;
        switch($response->getReceiver()) {
            case IReceiver::MYSELF:
                $from->getConnection()->send($message);
                $numRecv++;
                break;
            case IReceiver::ALL:
                foreach ($this->clients as $client) {
                    $client->getConnection()->send($message);
                    $numRecv++;
                }
                break;
            case IReceiver::ALL_EXCEPT_MYSELF:
                foreach ($this->clients as $client) {
                    if ($from->getConnection() !== $client->getConnection()) {
                        $client->getConnection()->send($message);
                        $numRecv++;
                    }
                }
                break;
            case IReceiver::DEFINED_LIST:
                $receivers = $response->getReceiverlist();
                foreach ($this->clients as $client) {
                    if ($client->isAuthenticated() && in_array($client->getUsername(),$receivers)) {
                        $client->getConnection()->send($message);
                        $numRecv++;
                    }
                }
                break;
            case IReceiver::FORBIDDEN:
                $message = json_encode(['command'=>'message','params'=>'Command forbidden.']);
                $from->getConnection()->send($message);
                $numRecv++;
                break;
            case IReceiver::TERMINATE_SERVER:
                call_user_func($this->stopCallBack);
                return;
                break;
            default:
                break;
        }

        if ($response) {
            if ($response->isAddToLog()) {
                Log::log(sprintf('Connection %d sending message "%s" to %d other connection%s'
                    , $from->getConnection()->resourceId, $message, $numRecv, $numRecv == 1 ? '' : 's'));
            }

        } else {
            Log::log(sprintf('Connection %d sending message "%s" to %d other connection%s'
                , $from->getConnection()->resourceId, $message, $numRecv, $numRecv == 1 ? '' : 's'));
        }

        $response=null;
        gc_collect_cycles();


    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->remove($conn->resourceId);

        Log::log("Connection {$conn->resourceId} has disconnected");
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        Log::log( "An error has occurred: {$e->getMessage()}");

        /*$this->clients->remove($conn->resourceId);

        $conn->close();*/
    }
};