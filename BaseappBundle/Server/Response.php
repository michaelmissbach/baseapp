<?php

namespace BaseApp\BaseappBundle\Server;

/**
 * class IResponse
 * @package BaseApp\BaseappBundle\Server
 */
class Response
{
    /**
     * @var IReceiver
     */
    protected $receiver;

    /**
     * @var []
     */
    protected $receiverlist;

    /**
     * @var string
     */
    protected $command;

    /**
     * @var mixed
     */
    protected $message;

    /**
     * @var bool
     */
    protected $addToLog = true;

    /**
     * Response constructor.
     * @param $receiver
     * @param string $command
     * @param string $message
     * @param bool $addToLog
     */
    public function __construct($receiver, $command = '', $message = '', $addToLog = true)
    {
        $this->receiver = $receiver;
        $this->command = $command;
        $this->message = $message;
        $this->addToLog = $addToLog;
    }

    /**
     *
     */
    public function __destruct()
    {
        $this->receiver = null;
        $this->receiverlist = null;
        $this->message = null;
        $this->command = null;
    }

    /**
     * @return string
     */
    public function getReceiver(): string
    {
        return $this->receiver;
    }

    /**
     * @param string $receiver
     * @return Response
     */
    public function setReceiver(string $receiver): Response
    {
        $this->receiver = $receiver;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReceiverlist()
    {
        return $this->receiverlist;
    }

    /**
     * @param mixed $receiverlist
     * @return Response
     */
    public function setReceiverlist($receiverlist)
    {
        $this->receiverlist = $receiverlist;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return Response
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @param string $command
     * @return Response
     */
    public function setCommand(string $command): Response
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAddToLog(): bool
    {
        return $this->addToLog;
    }

    /**
     * @param bool $addToLog
     * @return Response
     */
    public function setAddToLog(bool $addToLog): Response
    {
        $this->addToLog = $addToLog;
        return $this;
    }
}