<?php

namespace BaseApp\BaseappBundle\Server\Handler;

use BaseApp\BaseappBundle\Server\Connection;
use BaseApp\BaseappBundle\Server\IReceiver;
use BaseApp\BaseappBundle\Server\Response;

/**
 * Class Broadcast
 * @package BaseApp\BaseappBundle\Server
 */
class Broadcast implements IHandler
{
    /**
     * @return string
     */
    public function getCommand()
    {
        return 'broadcast';
    }

    /**
     * @param Connection $from
     * @param $params
     * @param $clients
     * @return Response|mixed
     */
    public function execute(Connection $from, $params,$clients)
    {
        return new Response(IReceiver::ALL_EXCEPT_MYSELF,'message',$params['message']);
    }
}
