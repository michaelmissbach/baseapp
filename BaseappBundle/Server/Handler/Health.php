<?php

namespace BaseApp\BaseappBundle\Server\Handler;

use BaseApp\BaseappBundle\Server\Connection;
use BaseApp\BaseappBundle\Server\IReceiver;
use BaseApp\BaseappBundle\Server\Log;
use BaseApp\BaseappBundle\Server\Response;

/**
 * Class Health
 * @package BaseApp\BaseappBundle\Server
 */
class Health implements IHandler
{
    /**
     * @var int
     */
    protected $start = 0;

    /**
     * Health constructor.
     */
    public function __construct()
    {
        $this->start = time();
    }
    /**
     * @return string
     */
    public function getCommand()
    {
        return 'health';
    }

    /**
     * @param Connection $from
     * @param $params
     * @param $clients
     * @return Response|mixed
     */
    public function execute(Connection $from, $params, $clients)
    {
        if (!in_array('admin',$from->getGroups())) {
            return new Response(IReceiver::FORBIDDEN);
        }

        $result = [
            'cpu_load' => sys_getloadavg()[0],
            'client_count' => count($clients),
            'start' => date(DATE_ATOM,$this->start),
            'memory_limit' => $this->unitToInt(ini_get('memory_limit')),
            'memory_usage_percent'=> (int)(memory_get_usage()/$this->unitToInt(ini_get('memory_limit'))*100),
            'memory_usage'=>memory_get_usage(),
            'memory_peak'=>memory_get_peak_usage(),
        ];

        //foreach($result as $key=>$value) {
        //    \BaseApp\BaseappBundle\Server\Log::log(sprintf('%s : %s',$key,$value));
        //}

        return new Response(
            IReceiver::MYSELF,
            'health',
            $result,
            false
        );
    }

    /**
     * @param $s
     * @return int
     */
    protected function unitToInt($s)
    {
        return (int)preg_replace_callback('/(\-?\d+)(.?)/', function ($m) {
            return $m[1] * pow(1024, strpos('BKMG', $m[2]));
        }, strtoupper($s));
    }
}
