<?php

namespace BaseApp\BaseappBundle\Server\Handler;

use BaseApp\BaseappBundle\Server\Connection;
use BaseApp\BaseappBundle\Server\IReceiver;
use BaseApp\BaseappBundle\Server\Response;

/**
 * Class Broadcast
 * @package BaseApp\BaseappBundle\Server
 */
class Log implements IHandler
{
    /**
     * @return string
     */
    public function getCommand()
    {
        return 'log';
    }

    /**
     * @param Connection $from
     * @param $params
     * @param $clients
     * @return Response|mixed
     */
    public function execute(Connection $from, $params, $clients)
    {
        if (!in_array('admin',$from->getGroups())) {
            return new Response(IReceiver::FORBIDDEN);
        }

        switch(true) {
            case isset($params['method']) && $params['method']==="clear":
                \BaseApp\BaseappBundle\Server\Log::clear();
                $message = 'Log cleared.';
                break;
            default:
                $message = \BaseApp\BaseappBundle\Server\Log::get();
        }

        return new Response(IReceiver::MYSELF,'log',$message,false);
    }
}
