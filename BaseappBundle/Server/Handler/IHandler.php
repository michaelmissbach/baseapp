<?php

namespace BaseApp\BaseappBundle\Server\Handler;

use BaseApp\BaseappBundle\Server\Connection;
use BaseApp\BaseappBundle\Server\IReceiver;
use BaseApp\BaseappBundle\Server\Response;

/**
 * Interface IHandler
 * @package BaseApp\BaseappBundle\Server
 */
interface IHandler
{
    /**
     * @return string
     */
    public function getCommand();

    /**
     * @param Connection $from
     * @param $params
     * @param $clients
     * @return mixed
     */
    public function execute(Connection $from,$params,$clients);
}