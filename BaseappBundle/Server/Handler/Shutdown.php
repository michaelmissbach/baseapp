<?php

namespace BaseApp\BaseappBundle\Server\Handler;

use BaseApp\BaseappBundle\Server\Connection;
use BaseApp\BaseappBundle\Server\IReceiver;
use BaseApp\BaseappBundle\Server\Log;
use BaseApp\BaseappBundle\Server\Response;

/**
 * Class Shutdown
 * @package BaseApp\BaseappBundle\Server
 */
class Shutdown implements IHandler
{
    /**
     * @return string
     */
    public function getCommand()
    {
        return 'shutdown';
    }
    
    /**
     * @return Response
     */
    public function execute(Connection $from, $params, $clients)
    {
        if (!in_array('admin',$from->getGroups())) {
            return new Response(IReceiver::FORBIDDEN);
        }

        \BaseApp\BaseappBundle\Server\Log::log('Forced shutdown from web.');
        return new Response(IReceiver::TERMINATE_SERVER,'','');
    }
}
