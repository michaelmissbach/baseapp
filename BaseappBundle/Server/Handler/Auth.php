<?php

namespace BaseApp\BaseappBundle\Server\Handler;

use BaseApp\BaseappBundle\Entity\Groups;
use BaseApp\BaseappBundle\Entity\User;
use BaseApp\BaseappBundle\Server\Connection;
use BaseApp\BaseappBundle\Server\IReceiver;
use BaseApp\BaseappBundle\Server\Response;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class Auth
 * @package BaseApp\BaseappBundle\Server
 */
class Auth implements IHandler
{
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * Auth constructor.
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return 'auth';
    }

    /**
     * @param Connection $from
     * @param $params
     * @param $clients
     * @return Response|mixed
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     */
    public function execute(Connection $from, $params, $clients)
    {
        $user = $this->doctrine->getRepository(User::class)->findOneBy(['username'=>$params['username'],'token'=>$params['token']]);

        if ($user) {
            $from->setAuthenticated(true);
            $from->setUsername($user->getUsername());

            $groups = [];
            /** @var Groups $group */
            foreach($user->getGroups() as $group) {
                $groups[] = $group->getInterngroupname();
            }
            $from->setGroups($groups);

            $this->doctrine->getManager()->detach($user);
            $this->doctrine->getManager()->clear();

            \BaseApp\BaseappBundle\Server\Log::log(sprintf('User "%s" authenticated.',$user->getUsername()));
            \BaseApp\BaseappBundle\Server\Log::log(sprintf('User "%s" has groups: %s.',$user->getUsername(),implode(',',$groups)));
        } else {
            \BaseApp\BaseappBundle\Server\Log::log('Tried to authenticate but failed.');
        }

        /**
         * command ist die javascript function
         * params sind die return parameter
         */

        return new Response(IReceiver::MYSELF,'auth',(bool)$user);
    }
}
