<?php

namespace BaseApp\BaseappBundle\Server\Handler;

use BaseApp\BaseappBundle\Server\Connection;
use BaseApp\BaseappBundle\Server\IReceiver;
use BaseApp\BaseappBundle\Server\Response;

/**
 * Class DirectMessage
 * @package BaseApp\BaseappBundle\Server
 */
class DirectMessage implements IHandler
{
    /**
     * @return string
     */
    public function getCommand()
    {
        return 'directmessage';
    }

    /**
     * @param Connection $from
     * @param $params
     * @param $clients
     * @return Response|mixed
     */
    public function execute(Connection $from, $params, $clients)
    {
        $response = new Response(IReceiver::DEFINED_LIST,'message',$params['message']);
        $response->setReceiverlist($params['list']);
        return $response;
    }
}
