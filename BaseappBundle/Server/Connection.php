<?php

namespace BaseApp\BaseappBundle\Server;

use Ratchet\ConnectionInterface;

/**
 * Class Connection
 * @package BaseApp\BaseappBundle\Server
 */
class Connection
{
    /**
     * @var ConnectionInterface
     */
    protected $connection;

    /**
     * @var bool
     */
    protected $authenticated = false;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var array
     */
    protected $groups = [];

    /**
     * Connection constructor.
     * @param ConnectionInterface $connection
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return ConnectionInterface
     */
    public function getConnection(): ConnectionInterface
    {
        return $this->connection;
    }

    /**
     * @param ConnectionInterface $connection
     * @return Connection
     */
    public function setConnection(ConnectionInterface $connection): Connection
    {
        $this->connection = $connection;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAuthenticated(): bool
    {
        return $this->authenticated;
    }

    /**
     * @param bool $authenticated
     * @return Connection
     */
    public function setAuthenticated(bool $authenticated): Connection
    {
        $this->authenticated = $authenticated;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Connection
     */
    public function setUsername(string $username): Connection
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return array
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param array $groups
     * @return Connection
     */
    public function setGroups(array $groups): Connection
    {
        $this->groups = $groups;
        return $this;
    }
}