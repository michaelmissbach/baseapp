<?php

namespace BaseApp\BaseappBundle\Server;

/**
 * Interface IReceiver
 * @package BaseApp\BaseappBundle\Server\Connection
 */
interface IReceiver
{
    const ALL = 'all';

    const ALL_EXCEPT_MYSELF = 'all_except_myself';

    const MYSELF = 'myself';

    const DEFINED_LIST = 'defined_list';

    const FORBIDDEN = 'forbidden';

    const TERMINATE_SERVER = 'terminate_server';
}
