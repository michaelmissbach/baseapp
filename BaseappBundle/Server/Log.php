<?php

namespace BaseApp\BaseappBundle\Server;

class Log
{
    public static $file = 'logfile_%s.txt';

    public static $path = '';

    public static $bufferLines = 100;

    public static $buffer = [];

    public static $port = '';

    public static function setPort($port)
    {
        self::$port = $port;
    }

    protected static function getFilename()
    {
        return sprintf(self::$path.self::$file,self::$port);
    }

    public static function clear()
    {
        try {
            unlink(self::getFilename());
        }catch(\Exception $e) {
            Log::log($e->getMessage());
        }

    }

    public static function get()
    {
        return file_get_contents(self::getFilename());
    }

    public static function log($message)
    {
        //echo $message.PHP_EOL;

        self::$buffer[] = sprintf('%s: %s',date(DATE_ATOM,time()),$message);

        if (count(self::$buffer) > self::$bufferLines) {
            self::write();
        }
    }

    /**
     *
     */
    public static function write()
    {
        if (count(self::$buffer)) {
            file_put_contents(self::getFilename(),implode(self::$buffer,PHP_EOL).PHP_EOL,FILE_APPEND);
            self::$buffer = null;
            self::$buffer = [];
        }
    }
}
