<?php

namespace BaseApp\BaseappBundle\Repository;

use Doctrine\ORM\EntityRepository;
use BaseApp\BaseappBundle\Entity\UserDetail;

/**
 * @method UserDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserDetail[]    findAll()
 * @method UserDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDetailRepository extends EntityRepository
{    
}
