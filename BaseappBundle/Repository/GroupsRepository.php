<?php

namespace BaseApp\BaseappBundle\Repository;

use Doctrine\ORM\EntityRepository;
use BaseApp\BaseappBundle\Entity\Groups;

/**
 * @method Groups|null find($id, $lockMode = null, $lockVersion = null)
 * @method Groups|null findOneBy(array $criteria, array $orderBy = null)
 * @method Groups[]    findAll()
 * @method Groups[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupsRepository extends EntityRepository
{    
}
