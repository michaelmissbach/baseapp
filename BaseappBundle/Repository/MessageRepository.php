<?php

namespace BaseApp\BaseappBundle\Repository;

use Doctrine\ORM\EntityRepository;
use BaseApp\BaseappBundle\Entity\Message;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends EntityRepository
{
   /**
     * @param $userid
     * @return array
     */
    public function getAll($userid)
    {
        $querybuilder = $this->createQueryBuilder('m');
        $querybuilder
            ->select('m')
            ->where('m.user = :userid')
            ->setParameter('userid',$userid)
            ->orderBy('m.sendAt','DESC')
        ;

        return $querybuilder->getQuery()->getResult();
    }

    /**
     * @param $userid
     * @return array
     */
    public function getUnread($userid,$limit = null)
    {
        $querybuilder = $this->createQueryBuilder('m');
        $querybuilder
            ->select('m')
            ->where('m.user = :userid')
            ->andWhere('m.unread = :unread')
            ->setParameter('userid',$userid)
            ->setParameter('unread',true)
            ->orderBy('m.sendAt','DESC')
        ;
        if ($limit) {
            $querybuilder->setMaxResults($limit);
        }

        return $querybuilder->getQuery()->getResult();
    }

    /**
     * @param $userid
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUnreadCount($userid)
    {
        $querybuilder = $this->createQueryBuilder('m');
        $querybuilder
            ->select('count(m.id)')
            ->where('m.user = :userid')
            ->andWhere('m.unread = :unread')
            ->setParameter('userid',$userid)
            ->setParameter('unread',true)
        ;

        return $querybuilder->getQuery()->getSingleScalarResult();
    }
}
