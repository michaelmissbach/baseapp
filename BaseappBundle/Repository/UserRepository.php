<?php

namespace BaseApp\BaseappBundle\Repository;

use Doctrine\ORM\EntityRepository;
use BaseApp\BaseappBundle\Entity\User;
/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends EntityRepository
{    
}
