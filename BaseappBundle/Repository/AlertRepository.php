<?php

namespace BaseApp\BaseappBundle\Repository;

use BaseApp\BaseappBundle\Entity\Alert;
use Doctrine\ORM\EntityRepository;

/**
 * @method Alert|null find($id, $lockMode = null, $lockVersion = null)
 * @method Alert|null findOneBy(array $criteria, array $orderBy = null)
 * @method Alert[]    findAll()
 * @method Alert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlertRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        $querybuilder = $this->createQueryBuilder('a');
        $querybuilder
            ->select('a')
            ->orderBy('a.createdAt','DESC')
        ;

        return $querybuilder->getQuery()->getArrayResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCount()
    {
        $querybuilder = $this->createQueryBuilder('a');
        $querybuilder
            ->select('count(a.id)')
        ;

        return $querybuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @return mixed
     */
    public function getHeader($maxResults = 3)
    {
        $querybuilder = $this->createQueryBuilder('a');
        $querybuilder
            ->select('a')
            ->orderBy('a.createdAt','DESC')
            ->setMaxResults($maxResults)
        ;

        return $querybuilder->getQuery()->getResult();
    }

    /**
     * @param $type
     * @param $message
     * @return Alert
     * @throws \Exception
     */
    public function createNew($type,$message)
    {
        $alert = new Alert();
        $alert->setContent($message);
        $alert->setType($type);

        return $alert;
    }
}
